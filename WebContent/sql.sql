-- ********** UserLogin **********
drop table UserLogin;

create table UserLogin(
	UserKey VARCHAR(150) not null primary key,
	UserID VARCHAR(150) not null unique,
	Password VARCHAR(150) not null
);

select * from USERLOGIN;




-- ********** UserLogin insert Sequnce **********
create sequence UserLogin_seq increment by 1 start with 1;
drop sequence UserLogin_seq;

alter table UserLogin drop constraint pk_UserLogin cascade;




-- ********** UserDetail **********
drop table UserDetail;

create table UserDetail(
	UserKey VARCHAR2(150) not null primary key,
	UserName VARCHAR2(15) not null,
	UserMail VARCHAR2(40) unique,
	UserPhone number(11) unique,
	UserArea VARCHAR2(6),
	UserGender number(1),
	UserType VARCHAR2(150) not null
);

--pk
ALTER TABLE UserDetail
ADD CONSTRAINT FK_UserKey FOREIGN KEY(UserKey)
REFERENCES UserLogin(UserKey);

select * from UserDetail;


insert into UserLogin values ('1','1','1');
insert into UserLogin (userID, password,userkey) values ('1','1','1');
insert into UserDetail values ('1','1',null,null,null,null,'1');

delete from UserLogin where userkey='2';
delete from USERDETAIL where userkey='2';




-- ********** Followers **********
drop table Followers;

create table Followers(
	myName VARCHAR2(15) not null,
	firendName VARCHAR2(15) not null
);

select * from Followers;




-- ********** Posts **********
drop table Posts;

create table Posts(
	postIdx number(8) not null,
	UserKey VARCHAR2(150) not null,
	postUserName VARCHAR2(40) not null,
	postContent VARCHAR2(200),
	postDate date default sysdate,
	postLike number(8) not null,
	postOriIdx number(8) default 0,
	postImgCheck number(1) default 0,
	constraint pk_posts_postIdx primary key (postIdx)
);

-- seuesnce
create sequence seq_postIdx Start with 1 increment by 1 ;

drop sequence seq_postIdx;

insert into Posts (postIdx,	UserKey, postUserName, postContent, postLike) 
	values (seq_postIdx.nextval,'1','1','1',0);

select * from Posts;
select * from posts order by postIdx desc;




-- ********** reply **********
drop table reply;

create table reply(
	replyIdx number(8) not null primary key,
	replyPostIdx number(8) not null,
	UserKey VARCHAR2(150) not null,
	replyUserName VARCHAR2(40) not null,
	replyContent varchar2(3000) not null,
	replyDate date default sysdate,
	replyLike number(8) default 0,
	replyRoot number(8) default 0,
	replyStep number(8) default 0,
	replyIndent number(1) default 0,
	constraint fk_replyPostIdx
	foreign key (replyPostIdx)
	references Posts (postIdx)
	on delete cascade
);

select * from reply;

create sequence reply_seq increment by 1 start with 1;
drop sequence reply_seq;

select * from reply where replyRoot = ? and 1 < replyStep

-- ********** like **********
drop table likeList;

create table likeList(
	UserKey VARCHAR(150) not null,
	pIdx number(8) not null
);

select * from likeList;




-- ********** img **********
drop table img;

create table img(
	path VARCHAR(300) not null,
	pIdx number(8) not null,
	constraint fk_img_pIdx
	foreign key (pIdx)
	references Posts (postIdx)
	on delete cascade
);

select * from img;


