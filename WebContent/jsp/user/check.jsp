<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
<title>check</title>
</head>
<body>
	<%-- <%
		String id = request.getAttribute("userId");
		String name = request.getAttribute("userName");
	
	%> --%>
	<%
		String id = (String)request.getAttribute("userId");
		String name = (String)request.getAttribute("userName");
		int check = (int)request.getAttribute("check");
	
		pageContext.setAttribute("id", id);
		pageContext.setAttribute("check", check);
		pageContext.setAttribute("name", name);
	%>
	<c:if test="${ id != null && !''.equals(id) }">
		<c:choose >
			<c:when test="${check == 1}">
				<font size="2">${id}는 이미 사용중입니다</font>
											<!-- <form action="b.jsp">
												<input name="userID">
												<input type="submit" value="중복확인">
											</form> -->
				<input type="button" value="닫기" onclick="windowCloseName()">
			</c:when>
			<c:otherwise>
				<font size="2">${id}는 사용가능합니다</font>
				<input type="button" value="닫기" onclick="windowCloseId()">
			</c:otherwise>	
		</c:choose>
	</c:if>
	<c:if test="${ name != null && !''.equals(name) }">
		<c:choose>
			<c:when test="${check == 1}">
				<font size="2">${name}는 이미 사용중입니다</font>
				<input type="button" value="닫기" onclick="windowCloseName()">
												<!-- <form action="b.jsp">
													<input name="userID">
													<input type="submit" value="닉네임확인" onclick="widnowCloseName()">
												</form> -->
			</c:when>
			<c:otherwise>
					<font size="2">${name}는 사용가능합니다</font>
					<input type="button" value="닫기" onclick="windowCloseName()">
			</c:otherwise>
		</c:choose>
	</c:if>
	<script>
		function windowCloseId(){
			/* opener.document.joinform.userID.value="${id}"; */
			window.close();
		}
		
		function windowCloseName(){
			window.close();
			//var fo=opener.document.joinform;
			//fo.userName.value="111";
			//self.close();
		}
	</script>
</body>
</html>