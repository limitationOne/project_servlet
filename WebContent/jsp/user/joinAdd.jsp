<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>joinAdd</title>
<style type="text/css">
	body > div:FIRST-CHILD { margin: auto; width: 400px; }
	h1 { text-align: center; }
	input, select { width: 100%; margin-top: 10px; padding: 5px;}
</style>
</head>
<body>
	<div>
		<h1>회원가입 추가정보 입력</h1>
		<form action="../user/joinAdd.do" method="post">
			<input name="userMail" placeholder="mail" autocomplete="off"/><br/>
			<input name="userPhone" placeholder="phone" autocomplete="off"/><br/>
			<select name="userArea">
				<option value="서울">서울</option>
				<option value="대전">대전</option>
				<option value="대구">대구</option>
				<option value="부산">부산</option>
				<option value="울산">울산</option>
				<option value="광주">광주</option>
				<option value="경기">경기</option>
				<option value="전남">전라남도</option>
				<option value="전북">전라북도</option>
				<option value="충남">충청남도</option>
				<option value="충북">충청북도</option>
				<option value="경남">경상남도</option>
				<option value="경북">경상북도</option>
				<option value="강원">강원도</option>
			</select><br/>
			<select name="userGender">
				<option value="1">남자</option>
				<option value="2">여자</option>
			</select><br/>
			<input type="submit" value="확인"/><br/>
			<input type="button" value="다음에 입력하기" onclick="location.href='../user/login.jsp'"/>
		</form>
	</div>
</body>
</html>