<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
	body > div:FIRST-CHILD { margin: auto; width: 400px; }
	h1 { text-align: center; }
	input, select { width: 100%; margin-top: 10px; padding: 5px; height:30px;}
	h2{width:100%; height:50px; text-align:center; color:red;}
</style>
<title>memberUpdate</title>
</head>
<body>
	<div>
		<form action="../user/memberUpdate.do" method="post">
			회원명: ${userName}<br>
			성별: ${userGender}<br>
			메일: <input name="userMail" value="${userMail}"><br>
			주소: <select name="userArea">
					<option value="서울">서울</option>
					<option value="대전">대전</option>
					<option value="대구">대구</option>
					<option value="부산">부산</option>
					<option value="울산">울산</option>
					<option value="광주">광주</option>
					<option value="경기">경기</option>
					<option value="전남">전라남도</option>
					<option value="전북">전라북도</option>
					<option value="충남">충청남도</option>
					<option value="충북">충청북도</option>
					<option value="경남">경상남도</option>
					<option value="경북">경상북도</option>
					<option value="강원">강원도</option>
				</select><br>
			연락처: <input name="userPhone" type="number" value="${userPhone }"><br>
				<input type="submit" value="확인">
				<input type="button" value ="취소" onclick="location.href='../user/member.do'">
		</form>
	</div>
</body>
</html>