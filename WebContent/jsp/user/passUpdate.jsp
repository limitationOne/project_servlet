<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
	body > div:FIRST-CHILD { margin: auto; width: 400px; }
	h1 { text-align: center; }
	input { width: 100%; margin-top: 10px; padding: 5px; height:30px;}
	h2{width:100%; height:50px; text-align:center; color:red;}
</style>
<title>passUpdate</title>
</head>
<body>
	<div>
		<h1>비밀번호 변경</h1>
		<form action="../user/passUpdate.do" method="post">
			<input name="oldPass" placeholder="기존 비밀번호"><br>
			<input name="newPass1" placeholder="새로운 비밀번호"><br>
			<input name="newPass2" placeholder="새로운 비밀번호 확인"><br>
			<input type="submit" value ="확인">
			<input type="button" value ="취소" onclick="location.href='../user/member.do'">
		</form>
		${alarm}<br>
	</div>
</body>
</html>