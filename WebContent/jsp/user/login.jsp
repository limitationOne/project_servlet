<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>login</title>
<style type="text/css">
	body > div:FIRST-CHILD { margin: auto; width: 400px; }
	h1 { text-align: center; }
	input { width: 100%; margin-top: 10px; padding: 5px; height:30px;}
	h2{width:100%; height:50px; text-align:center; color:red;}
</style>
</head>
<body>
	<jsp:include page="../include/noLoginCheck.jsp"/>
	<div>
		<h1>로그인</h1>
		<form action="../user/login.do" method="post">
			<input type="text" name="userId" placeholder="ID" autocomplete="off"/><br/>
			<input type="password" name="password" placeholder="Password" autocomplete="off"/><br/>
			<input type="submit" value="Login"/>
			<input type="button" value="회원가입" onclick="location.href='../user/join.jsp'"/>
		</form>
		<h2>${alarm}</h2>
	</div>
</body>
</html>