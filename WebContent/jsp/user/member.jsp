<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
	body > div:FIRST-CHILD { margin: auto; width: 400px; }
	h1 { text-align: center; }
	input { width: 100%; margin-top: 10px; padding: 5px; height:30px;}
	h2{width:100%; height:50px; text-align:center; color:red;}
</style>
<title>member</title>
</head>
<body>
	<div>
		<ul>
			<li>회원명: <span>${member.userName }</span></li>
			<li>성별: <span>${member.userGender}</span></li>
			<li>메일: <span>${member.userMail }</span></li>
			<li>연락처: <span>${member.userPhone }</span> </li>
			<li>주소: <span>${member.userArea}</span> </li>
			<li>
				<input type="button" value ="메인페이지" onclick="location.href='../page/myMain.do'">
				<input type="button"  value="비번수정" onclick="location.href='../user/passUpdate.jsp'">	
				<input type="button" value="회원정보수정" onclick="location.href='../user/memberUpdate.jsp'">
				<input type="button" value="회원탈퇴" onclick="location.href='../user/deletMember.jsp'">
			</li>
		</ul>
	</div>
</body>
</html>