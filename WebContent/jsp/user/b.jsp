<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>b</title>
</head>
<body>
	<% 
		String userId = request.getParameter("userID");
		String userName = request.getParameter("userName");
		String smake =request.getParameter("make");
		int make = Integer.parseInt(smake);
		request.setAttribute("userId", userId);
		request.setAttribute("userName", userName);
		// page 처리
		pageContext.setAttribute("make", make);
		
		if(make == 0 && userId != null && !"".equals(userId) ){
			RequestDispatcher dis = request.getRequestDispatcher("../user/checkid.do");
			dis.forward(request, response);
		}else if(make == 1 && userName != null && !"".equals(userName) ){
			RequestDispatcher dis = request.getRequestDispatcher("../user/checkName.do");
			dis.forward(request, response);
		}
	%>
	<%-- 
	<c:if test="${ make == 0 && userId != null && userId != '' }">
		<jsp:forward page="checkid.do"/>
	</c:if>
	<c:if test="${ make == 1 && userName != null && userName != '' }">
		<jsp:forward page="checkname.do">
	</c:if> 
	--%>
</body>
</html>