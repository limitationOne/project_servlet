<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
	body > div:FIRST-CHILD { margin: auto; width: 400px; }
	h1 { text-align: center; }
	input { width: 62%; margin-top: 10px; padding: 5px; height:30px;}
	.joinBtn{ width: 30%; }
	.joinBtn1{ width: 47%; }
	h2{width:100%; height:50px; text-align:center; color:red;}
</style>
<title>join</title>
</head>
<body>
	<jsp:include page="../include/noLoginCheck.jsp"/>
	<div>
		<h1>회원가입</h1>
		<form action="../user/join.do" method="post">
			<input type="text" name="userID" placeholder="아이디" autocomplete="off"/>&nbsp;&nbsp;<input class="joinBtn" type="button" value="id중복체크" onclick="openConfirmId(this.form)"><br>
			<input type="password" name="password" placeholder="비번"><br>
			<input type="text" name="userName" placeholder="닉네임" autocomplete="off"/>&nbsp;&nbsp;<input class="joinBtn" type="button" value="닉네임중복체크" onclick="openConfirmName(this.form)"><br>
			<input class="joinBtn1" type="submit" value="확인">
			<input class="joinBtn1" type="button" value="취소" onclick="location.href='../user/login.jsp'">
		</form>
	</div>
	<script>
		function openConfirmId(joinform){
			var id = joinform.userID.value;
			var make = 0;
			var url="b.jsp?userID="+id+"&make="+make;
			// 닉네임 and 아이디 빈칸일때 알림을 주는 기능
			if(id.length==0){
				alert("아이디를 입력하세요")
				joinform.userID.focus();
				return false;
			}
			open(url,'check', 'width=500 ,height=300');
		}
		
		function openConfirmName(joinform){
			
			var name= joinform.userName.value;
			var make = 1;
			var url="b.jsp?userName="+name+"&make="+make;
			if(name.length ==0){
				alert("닉넴을 입력하세요");
				joinform.userName.focus();
				return false;
			}
			open(url,'check1','width=500, height=300');			
		}
	</script>
	<h2>${alarm}</h2>
</body>
</html>