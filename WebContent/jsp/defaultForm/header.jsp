<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<link rel="stylesheet" type="text/css" href="../../css/header.css">
<jsp:include page="../include/loginCheck.jsp"/>
<header>
	<h1 class="titleLogo" onclick="location.href='../page/myMain.do'">OutStar</h1>
		<!-- 회원정보 -->
		<div class="userInfo">
			${ sessionScope.userName }님 반갑습니다.<br/>
			<input type="button" value="정보보기" onclick="location.href='../user/member.do'"/>
			<!-- 로그아웃 -->
			<input type="button" value="로그아웃" onclick="location.href='../user/logout.do'">
		</div>
</header>
<nav>
	<!-- 메인 -->
	<div class="navMenu">
		<input class="notInput" type="button" value="전체글" onclick="location.href='../page/myMain.do'"/>
	</div>
</nav>