<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<style type="text/css">
	aside { float: left; width: 20%; padding: 10px; }
</style>
<aside>
	<!-- 글쓰기 -->
	<a href="../page/postWrite.jsp">글쓰기</a><br/>
	<!-- 친구찾기 -->
	<a href="" onclick="alert('준비중');">친구찾기</a><br/>
	<!-- 팔로우 리스트 -->
	<a href="" onclick="alert('준비중');">팔로우 리스트</a><br/>
	<!-- 좋아요 글 리스트 -->
	<a href="" onclick="alert('준비중');">좋아요 글 리스트</a><br/>
	<!-- 작성한글 리스트 -->
	<input class="notInput" type="button" value="작성한 글" onclick="location.href='../page/postList.po'"/><br/>
	<!-- 작성한 리플 리스트 -->
	<input class="notInput" type="button" value="작성한 리플 리스트" onclick="myReply(this);"><br/>
</aside>
<script>
	//댓글 로드
	function myReply(obj){
		var article = $('#mainArticle');
		
		var xhr = new XMLHttpRequest();
		
		xhr.onreadystatechange = function(data){
			if(xhr.readyState == 4 && xhr.status == 200){
				var list = JSON.parse(xhr.responseText);
				
				article.empty();
				
				for(var idx in list){
					replyDivAdd(article, list[idx], $(obj).val());
				}
			}
		}
		
		xhr.open('post', '../reply/myReplyList.re', true);
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
		
		xhr.send(null);
	}
</script>