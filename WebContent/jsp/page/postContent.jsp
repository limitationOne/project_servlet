<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<title>postContent</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../css/postContentUpdate.css">
<c:forEach items="${ postList }" var="dto">
	<section>
		<div class="postDiv">
			<div class="postHead">
				<form class="contentForm" action="../page/postList.po" method="post">
					<input type="hidden" name="postIdx" value="${ dto.postIdx }"/>
					<input type="hidden" name="userKey" value="${ dto.userKey }"/>
					<input type="submit" value="${ dto.postUserName }"/>
					<c:if test="${ sessionScope.userKey == dto.userKey }">
						<input class="postDeleteBtn" type="button" value="X" onclick="postDeleteCheck(${ dto.postIdx })"/>
						<input class="postDeleteBtn" type="button" value="수정" onclick="location.href='../page/postUpdateUI.po?postIdx=${ dto.postIdx }'"/>
					</c:if>
					<div>${ dto.postDate }</div>
				</form>
			</div>
			<div class="postContent">
				<c:forEach items="${ dto.imgList }" var="imgList">
					<img alt="" src="${ imgList.path }"> <br/>
				</c:forEach>
				${ dto.postContent }<br/>
			</div>
			<div class="postFoot">
				<input type="button" value="좋아요b ${ dto.postLike }" onclick="likeAdd(this);"/>
				<input type="button" value="댓글보기" onclick="loadReply(this);" style="float: right;"/>
				<form action="../page/postCopy.po" method="post">
					<input type="hidden" name="postIdx" value="${ dto.postIdx }"/>
					<c:if test="${ dto.userKey != sessionScope.userKey }">
						<input type="submit" value="퍼가기"/>
					</c:if>
					<c:if test="${ dto.postOriIdx != null && dto.postOriIdx != 0 }">
						<input type="button" value="출처" onclick="location.href='../page/postOriginal.po?postOriIdx=${ dto.postOriIdx }'"/>
					</c:if>
				</form>
				<div class="replyBody">
					<input type="hidden" name="postIdx" value="${ dto.postIdx }"/>
					<div class="replyList"></div>
					<input name="refresh" type="hidden" value="새댓글 보기" style="width: 100%;" onclick="refreshReply(this)"/>
					<div class="replyWriteDiv" style="display: none;"></div>
				</div>
			</div>
		</div>
	</section>
</c:forEach>
<script type="text/javascript">
	// 게시글 삭제
	function postDeleteCheck(postIdx){
		if(confirm('삭제하시겠습니까?')){
			location.href='../page/postDelete.po?postIdx=' + postIdx;
		}
	}
	
	// 좋아요
	function likeAdd(obj){
		if(confirm('좋아요b 하시겠습니까?')){
			var length = obj.parentNode.children.length;
			var url = '../page/';
			var idx;
			
			for(i = 0 ; i < length ; ++i) {
				var getTag = obj.parentNode.children[i];
				if(getTag.name == 'postIdx'){
					url += 'postAddLike.po';
					idx = getTag.value;
				}
			}
			
			$.ajax({
				type : "POST",
				url : url,
				data: "postIdx=" + idx,
				contentType: "application/x-www-form-urlencoded;charset=UTF-8",
				success: function(data) {
					if(data == null) return;
					if(data == 2) alert('안좋아요!');
					if(data == 1) alert('좋아요!');
					if(data == -1) alert('에러발생!');
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert("에러발생");
				}			
			});
		}
	}
	
	// 댓글 작성창 생성
	function writeReplyUI(obj, div, postIdx, replyIndent){
		var $divChildren 
				= $('<input type="hidden" name="postIdx" value="' + postIdx + '"/>' 
					+ '<textarea name="replyContent" rows="6" placeholder="내용을 입력해주세요." ' 
							+ 'style="margin-top: 5px; float: right; ' 
							+ 'width: ' + (98 - (replyIndent * 10)) + '%; "></textarea>' 
					+ '<input class="replyWriteOkBtn" type="button" value="댓글작성완료" ' 
							+ 'onclick="writeReply(this);"/>');
		div.prepend($divChildren);
	}
	
	// 댓글에 댓글 작성창 생성
	function reWriteReplyUI(obj){
		var addDiv = $(obj).parents('.replyfoot');
		var postIdx = $(obj).parents('.replyfoot').parents('.replyDiv')
				.parents('.replyList').parents('.replyBody').children('input[name=postIdx]').val();
		
		writeReplyUI(obj, addDiv, postIdx, 0);
		obj.remove();
	}
	
	// 댓글 Div 생성
	function replyDivAdd(addDiv, obj, value){
		var addDiv = $(addDiv);
		var url = '../reply/replyUpdate.re?postIdx=' + obj.replyIdx;
		var addReplyText = '';
		
		if(value == '댓글보기'){
			addReplyText = '<section style="float: right; width: ' + (97 - (obj.replyIndent * 5)) + '%; padding: 10px;">'
		}else if(value == '작성한 리플 리스트'){
			addReplyText = '<section style="float: right; width: 75%; padding: 10px;">'
		}
		addReplyText += '<div class="replyDiv" style="background-color: rgb(241, 196, 15); ' 
							+ 'margin-bottom: 10px; border: 1px solid black; border-radius: 5px;">'
							
							+ '<input type="text" name="replyIdx" value="' + obj.replyIdx + '"/>'
							+ '<input type="hidden" name="replyIndent" value="' + obj.replyIndent + '"/>'
							+ '<input type="hidden" name="replyRoot" value="' + obj.replyRoot + '"/>'
							+ '<input type="text" name="replyStep" value="' + obj.replyStep + '"/>'
							
							+ '<div class="replyHead" style="margin-bottom: 5px; padding: 5px;">'
								+ '<a href="../reply/replyList.po?userKey=' + obj.userKey + '">' + obj.replyUserName + '</a>'
							
								+ '<input class="replyDeleteBtn" type="button" value="X" ' 
										+ 'onclick="replyDeleteCheck(' + obj.replyIdx + ')" style="float: right; margin-left: 10px;"/>'
								
								+ '<input class="replyDeleteBtn" type="button" value="수정" '
										+ 'onclick="location.href=' +"url" + '" style="float: right; margin-left: 10px;"/>'
								
								+ '<div style="float: right; font-size: 12px;">' + obj.replyDate + '</div>'
							+ '</div>'
							
							+ '<div class="replyContent" style="background-color: white; padding: 10px;">' + obj.replyContent + '</div>'
							
							+ '<div class="replyfoot"style="padding: 5px;">'
								+ '<input type="button" value="댓글달기" onclick="reWriteReplyUI(this);"/>'
							+ '</div></div></section>';
		
		var $addReply = $(addReplyText);
		
		addDiv.append($addReply);
	}
	
	// 댓글 로드
	function loadReply(obj){
		var replyWriteDiv = $(obj).parents('.postFoot').children('.replyBody').children('.replyWriteDiv');
		var replyList = $(obj).parents('.postFoot').children('.replyBody').children('.replyList');
		var postIdx = $(obj).parents('.postFoot').children('.replyBody').children('[name=postIdx]').val();
		var refresh = $(obj).parents('.postFoot').children('.replyBody').children('[name=refresh]');
		
		if(replyWriteDiv.css('display') != 'none'){
			refresh.attr('type', 'hidden');
			replyWriteDiv.css('display','none');
			replyList.children().remove();
			replyWriteDiv.children().remove();
		}else{
			replyWriteDiv.css('display','block');
			$.ajax({
				type : 'post',
				url : '../reply/replyList.re',
				data : "postIdx=" + postIdx,
				datatype : "json",
				contentType : "application/x-www-form-urlencoded;charset=UTF-8",
				success : function(data){
					var list = JSON.parse(data);
					
					for(var idx in list){
						replyDivAdd(replyList, list[idx], $(obj).val());
					}
				},
				error : function(){
					alert('errer_0');
				}
			});
			refresh.attr('type', 'button');
			writeReplyUI(obj, replyWriteDiv, postIdx, 0);
		}
	}
	
	// 댓글 새로고침
	/*
	function refreshReply(obj){
		var replyWriteDiv = $(obj).parents('.replyBody').children('.replyWriteDiv');
		var replyList = $(obj).parents('.replyBody').children('.replyList');
		var postIdx = $(obj).parents('.replyBody').children('[name=postIdx]').val();
		
		var xhr = new XMLHttpRequest();
		
		xhr.onreadystatechange = function(data){
			if(xhr.readyState == 4 && xhr.status == 200){
				var list = JSON.parse(xhr.responseText);
				
				for(var idx in list){
					for(var i = idx ; i < replyList.children().length ; ++i){
						alert(replyList.children('.replyDiv').children('input[name=replyIdx]').val() + " / " + list[idx].replyIdx);
						
						// 현재 보이는 댓글과 DB에서 가저온 댓글을 순차적으로 비교(중복 없도록)
						// 현재 보이는 댓글의 순서 != 새로 가저온 댓글의 순서 -> 두 순서를 비교하여서 할 일을 정의
						
						if(replyList.children('.replyDiv').children('input[name=replyIdx]').val() == list[idx].replyIdx){
							alert('1111');
							// replyDivAdd(replyList, list[idx]);
						}
					}
				}
			}
		}
		
		xhr.open('POST', '../reply/replyList.re', true);
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
		
		xhr.send('postIdx=' + 47);
	}
	//*/
	
	// 댓글 작성
	function writeReply(obj){
		var replyContent = $(obj).prevAll('[name=replyContent]');
		var postIdx = $(obj).parents().children($('[name=postIdx]')).val();
		var replyIdx = $(obj).parents('.replyfoot').parents('.replyDiv').children('input[name=replyIdx]').val()
		var replyIndent = $(obj).parents('.replyfoot').parents('.replyDiv').children('input[name=replyIndent]').val()
		var replyRoot = $(obj).parents('.replyfoot').parents('.replyDiv').children('input[name=replyRoot]').val()
		var replyStep = $(obj).parents('.replyfoot').parents('.replyDiv').children('input[name=replyStep]').val()
		
		if(replyContent.val() == null || replyContent.val() == "") {
			alert("내용을 입력해 주세요");
			replyContent.focus();
			return;
		}
		
		$.ajax({
			type : "post",
			url : "../reply/replyWrite.re",
			data : {"replyContent":replyContent.val(), "postIdx":postIdx, 
					"replyIdx":replyIdx,	"replyIndent":replyIndent,
					"replyRoot":replyRoot,	"replyStep":replyStep},
			contentType: "application/x-www-form-urlencoded;charset=UTF-8",
			success : function(data){
				if(data == 1){
					alert("완료");
					replyContent.val('');
					$('html, body').animate({scrollTop : replyContent.offset().top}, 200);
				}else{
					alert('실패');
				}
			},
			error : function(){
				alert('errer_0');
			}
		});
	}
	
	// 댓글 수정
	
	
	// 댓글 삭제
	function replyDeleteCheck(replyIdx){
		var xhr = new XMLHttpRequest();
		
		xhr.onreadystatechange = function(data){
			if(xhr.readyState == 4 && xhr.status == 200){
				if(JSON.parse(xhr.responseText) == 1) alert('삭제완료');
				else alert('삭제실패');
			}
		}
		
		xhr.open('post', '../reply/replyDelete.re', true);
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
		xhr.send('replyIdx=' + replyIdx);
	}
</script>