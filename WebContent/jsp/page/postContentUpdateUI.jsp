<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>postWrite</title>
<link rel="stylesheet" type="text/css" href="../../css/postContentUpdate.css">
</head>
<body>
	<jsp:include page="../defaultForm/header.jsp"/>
	<jsp:include page="../defaultForm/aside.jsp"/>
	<section>
		<div class="postDiv">
			<div class="postHead">
				<h1>글수정</h1>
			</div>
			<form action="postUpdate.po" method="post">
				<input type="hidden" name="postIdx" value="${ postList[0].postIdx }"/>
				<div class="postContent">
					<textarea name="postContent" placeholder="내용">${ postList[0].postContent }</textarea>
				</div>
				<div class="postFoot">
					<input type="submit" value="확인"/>
					<input type="button" value="취소" onclick="location.href='../page/myMain.do'"/>
				</div>
			</form>
		</div>
	</section>
	<jsp:include page="../defaultForm/footer.jsp"/>
</body>
</html>