<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>myMain</title>
</head>
<body>
	<jsp:include page="../defaultForm/header.jsp"/>
	<jsp:include page="../defaultForm/aside.jsp"/>
	<article id="mainArticle">
		<jsp:include page="postContent.jsp"/>
	</article>
	<jsp:include page="../defaultForm/footer.jsp"/>
</body>
</html>