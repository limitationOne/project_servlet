<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>postWrite</title>
<link rel="stylesheet" type="text/css" href="../../css/postContentUpdate.css">
</head>
<body>
	<jsp:include page="../defaultForm/header.jsp"/>
	<jsp:include page="../defaultForm/aside.jsp"/>
	<section>
		<div class="postDiv">
			<div class="postHead">
				<h1>글쓰기</h1>
			</div>
			<form action="../page/postWrite.po" method="post" enctype="multipart/form-data">
				<div class="postContent">
					<textarea name="postContent" placeholder="내용"></textarea><br/>
					<input name="img1" type="file"><br/>
					<input name="img2" type="file"><br/>
					<input name="img3" type="file"><br/>
					<input name="img4" type="file"><br/>
					<input name="img5" type="file"><br/>
				</div>
				<div class="postFoot">
					<input type="submit" value="확인"/>
					<input type="button" value="취소" onclick="location.href='../page/myMain.do'"/>
				</div>
			</form>
		</div>
	</section>
	<jsp:include page="../defaultForm/footer.jsp"/>
</body>
</html>