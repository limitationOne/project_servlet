<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${ sessionScope.userType == null }">
	<jsp:forward page="../user/login.jsp"/>
</c:if>