package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outstar.user.DAO.OutStarUserDAO;
import outstar.util.ActionForward;

public class OutStarMemberUpdateCommand implements OutStarCommand {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServerException {
		 HttpSession session = request.getSession(false);
		 
		 String userKey = (String) session.getAttribute("userKey");
		 String userMail = request.getParameter("userMail");
		 String userArea = request.getParameter("userArea");
		 String suserPhone = request.getParameter("userPhone");
		 
		 int userPhone = Integer.valueOf(suserPhone);
		 
		 OutStarUserDAO dao = new OutStarUserDAO();
		 
		 dao.memberUpdate(userKey, userMail, userArea,userPhone);
		 
 		return new ActionForward(false, "member.do");
	}

}
