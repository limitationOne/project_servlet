package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.jasper.tagplugins.jstl.core.Out;

import outstar.user.DAO.OutStarUserDAO;
import outstar.user.DTO.UserDetailDTO;
import outstar.util.ActionForward;

public class OutStarDeletMemberCommand implements OutStarCommand {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServerException {
		HttpSession session = request.getSession(false);
		OutStarUserDAO dao = new OutStarUserDAO();
		if(session != null){
			String userKey = (String) session.getAttribute("userKey");
			System.out.println(userKey);
			dao.deletMember(userKey);
			session.invalidate();
		}
		return new ActionForward(true, "login.jsp");
	}

}
