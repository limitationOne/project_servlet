package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.user.DAO.OutStarUserDAO;
import outstar.util.ActionForward;

public class OutStarCheckId implements OutStarCommand {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServerException {
		// TODO Auto-generated method stub
		int check=0;
		String userId = (String) request.getAttribute("userId");
		OutStarUserDAO dao = new OutStarUserDAO();
		boolean scheck =dao.checkId(userId.replaceAll(" ", ""));
		if(scheck){
			check=1;
		}else{
			check=0;
		}
		
		request.setAttribute("userId", userId);
		request.setAttribute("userName", null);
		request.setAttribute("check", check);
		
		return new ActionForward(false,"check.jsp");
	}

}
