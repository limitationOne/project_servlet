package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import org.apache.jasper.tagplugins.jstl.core.Out;

import outstar.user.DAO.OutStarUserDAO;
import outstar.user.DTO.UserDTO;
import outstar.user.DTO.UserDetailDTO;
import outstar.util.ActionForward;

public class OutStarMemberCommand implements OutStarCommand{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServerException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		if( session != null){
			String userKey = (String) session.getAttribute("userKey");
			OutStarUserDAO dao = new OutStarUserDAO();
			UserDetailDTO member = dao.member(userKey);
			request.setAttribute("member", member);
			return new ActionForward(false,"member.jsp");
		}
		return new ActionForward(true, "login.jsp");
	}
}
