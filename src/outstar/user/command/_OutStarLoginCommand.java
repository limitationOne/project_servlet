package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outstar.user.DAO.OutStarUserDAO;
import outstar.user.DTO.UserDetailDTO;
import outstar.util.ActionForward;

public class _OutStarLoginCommand implements OutStarCommand {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServerException {
		/*
		String userId = request.getParameter("userId");
		String password = request.getParameter("password");
		
		OutStarUserDAO dao = new OutStarUserDAO();
		
		UserDetailDTO dto = dao.login(userId, password);
		
		if( dto == null || dto.getUserKey() == null || "".equals(dto.getUserKey().replaceAll(" ", "")) ){
			return new ActionForward(true, "../user/login.jsp");
		}
		HttpSession session = request.getSession();
		
		session.setAttribute("userKey", dto.getUserKey());
		session.setAttribute("userName", dto.getUserName());
		session.setAttribute("userType", dto.getUserType());
		
		return new ActionForward(false, "../page/myMain.jsp");
		//*/
		
		
		/*
		ActionForward af = new ActionForward(false, "login.jsp");
		String userId = request.getParameter("userId");
		String password = request.getParameter("password");
		
		OutStarUserDAO dao = new OutStarUserDAO();
		
		int isOk = dao.login(userId, password);
		
		if (isOk == 1) {
			String alarm = "아이디가 존재하지않습니다.";
			request.setAttribute("alarm", alarm);
			return af;
		} else if (isOk == 2) {
			String alarm = "비번이 틀렸습니다";
			request.setAttribute("alarm", alarm);
			return af;
		}
		int userKey = dao.userKey(userId);
		
		String userName = dao.userName(userKey);

		HttpSession session = request.getSession();

		session.setAttribute("userKey", userKey);
		session.setAttribute("userName", userName);
		session.setAttribute("userType", userType);
		
		af.setPath("myMain.jsp");
		
		return af;
		// */
		
		ActionForward af = new ActionForward(false, "login.jsp");
		String userId = request.getParameter("userId");
		String password = request.getParameter("password");
		
		OutStarUserDAO dao = new OutStarUserDAO();
		
		int isOk = dao.loginCheck(userId, password);
		
		if (isOk == 1) {
			String alarm = "아이디가 존재하지않습니다.";
			request.setAttribute("alarm", alarm);
			return af;
		} else if (isOk == 2) {
			String alarm = "비번이 틀렸습니다";
			request.setAttribute("alarm", alarm);
			return af;
		}
		UserDetailDTO dto = dao.login(userId, password);
		
		if( dto == null || dto.getUserKey() == null || "".equals(dto.getUserKey().replaceAll(" ", "")) ){
			return new ActionForward(true, "../user/login.jsp");
		}
		HttpSession session = request.getSession();
		
		session.setAttribute("userKey", dto.getUserKey());
		session.setAttribute("userName", dto.getUserName());
		session.setAttribute("userType", dto.getUserType());
		
		af.setPath("myMain.jsp");
		
		return af;
	}
}