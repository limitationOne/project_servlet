package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outstar.user.DAO.OutStarUserDAO;
import outstar.user.DTO.UserDetailDTO;
import outstar.util.ActionForward;

public class OutStarLoginCommand implements OutStarCommand {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServerException {
		
		ActionForward af = new ActionForward(false, "../user/login.jsp");
		
		String userId = request.getParameter("userId");
		String password = request.getParameter("password");
		OutStarUserDAO dao = new OutStarUserDAO();
		
		int isOk = dao.loginCheck(userId, password);
		
		if (isOk == 1) {
			String alarm = "���̵� ���������ʽ��ϴ�.";
			request.setAttribute("alarm", alarm);
			return af;
		} else if (isOk == 2) {
			String alarm = "����� Ʋ�Ƚ��ϴ�";
			request.setAttribute("alarm", alarm);
			return af;
		}
		String userKey = dao.userKey(userId);
		
		UserDetailDTO dto = dao.login(userId, password);
		
		request.getSession().invalidate();
		HttpSession session = request.getSession(true);
		
		session.setAttribute("userKey", userKey);
		session.setAttribute("userName", dto.getUserName());
		session.setAttribute("userType", dto.getUserType());
		/*ArrayList<PostDTO> list = new PostDAO().postList();
		
		request.setAttribute("postList", list);*/
		
		af.setPath("../page/myMain.do");
		
		return af;
	}
}