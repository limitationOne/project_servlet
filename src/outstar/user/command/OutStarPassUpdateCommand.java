package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outstar.user.DAO.OutStarUserDAO;
import outstar.util.ActionForward;

public class OutStarPassUpdateCommand implements OutStarCommand {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServerException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		if(session != null){
			String userKey = (String) session.getAttribute("userKey");
			String oldPass = request.getParameter("oldPass");
			String newPass1 = request.getParameter("newPass1");
			String newPass2 = request.getParameter("newPass2");
			
			if( userKey == null || "".equals(userKey) 
					|| oldPass == null || "".equals(oldPass) 
					|| newPass1 == null || "".equals(newPass1) 
					|| newPass2 == null || "".equals(newPass2)) {
				new ActionForward(true, "passUpdate.jsp");
			}
			
			OutStarUserDAO dao = new OutStarUserDAO();
			boolean passCheck = dao.passCheck(userKey,oldPass);
			if(passCheck){
				String alarm = "비번이 틀렸습니다.";
				request.setAttribute("alarm", alarm);
				return new ActionForward(false, "passUpdate.jsp");
				
			}else if(!newPass1.equals(newPass2)){
				String alarm = "두번의 새로입력한 비번이 서로 다릅니다";
				request.setAttribute("alarm",alarm );
				return new ActionForward(false, "passUpdate.jsp");
			}else if(!passCheck && newPass1.equals(newPass2)){
				dao.passUpdate(userKey,newPass1);
				return new ActionForward(true,"member.do");
			}	
		}
		return new ActionForward(true, "login.jsp");
	}

}
