package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.posts.DAO.PostDAO;
import outstar.util.ActionForward;

public class OutStarMyMainCommand implements OutStarCommand {
	
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServerException {
		
		request.setAttribute("postList", new PostDAO().postList());
		
		return new ActionForward(false, "../page/myMain.jsp");
	}
}