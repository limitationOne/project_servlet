package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.user.DAO.OutStarUserDAO;
import outstar.util.ActionForward;

public class OutStarJoinCommand implements OutStarCommand {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServerException {
		ActionForward af = new ActionForward(false, "join.jsp");
		
		String userID = request.getParameter("userID").replaceAll(" ", "");
		String password = request.getParameter("password").replaceAll(" ", "");
		String userName = request.getParameter("userName").replaceAll(" ", "");
		
		if(userID == ""){
			String alarm = "아이디를 입력하세요";
			request.setAttribute("alarm", alarm);
			return af;
		}else if(password == ""){
			String alarm = "비밀번호를 입력하세요";
			request.setAttribute("alarm", alarm);
			return af;
		}else if(userName == ""){
			String alarm = "닉네임을 입력하세요";
			request.setAttribute("alarm", alarm);
			return af;
		}
		OutStarUserDAO dao = new OutStarUserDAO();
		
		int check = dao.join(userID,password,userName);
		
		System.out.println(check);
		
		if(check == 1){
			String alarm ="아이디가 중복됩니다";
			request.setAttribute("alarm", alarm);
			return af;
		}else if(check ==2){
			String alarm ="닉네임이  중복됩니다";
			request.setAttribute("alarm", alarm);
			return af;
		}else if(check == 3){
			request.setAttribute("alarm", "가입실패!");
			return af;
		}
		af.setPath("joinAdd.jsp");
		return af;
	}
}