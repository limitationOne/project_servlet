package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.util.ActionForward;

public class OutStarLogoutCommand implements OutStarCommand {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServerException {
		
		request.getSession().invalidate();
		
		return new ActionForward(false ,"../user/login.jsp");
	}
}