package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.util.ActionForward;

public interface OutStarCommand  {
	
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError,ServerException;

}
