package outstar.user.command;

import java.io.IOError;
import java.rmi.ServerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.user.DAO.OutStarUserDAO;
import outstar.util.ActionForward;

public class OutStarMemberDeletCheckCommand implements OutStarCommand {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServerException {
		// TODO Auto-generated method stub
		String userId = request.getParameter("userId");
		String userPassword = request.getParameter("userPassword");
		System.out.println(userId);
		System.out.println(userPassword);
		OutStarUserDAO dao = new OutStarUserDAO();
		boolean isOk = dao.isOk(userId,userPassword);
		if(isOk){
			return new ActionForward(true, "deletMember.do");
		}
		return new ActionForward(false ,"member.do");
	}

}
