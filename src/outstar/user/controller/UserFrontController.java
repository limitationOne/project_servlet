package outstar.user.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.user.command.OutStarCheckId;
import outstar.user.command.OutStarCheckName;
import outstar.user.command.OutStarCommand;
import outstar.user.command.OutStarDeletMemberCommand;
import outstar.user.command.OutStarJoinCommand;
import outstar.user.command.OutStarLoginCommand;
import outstar.user.command.OutStarLogoutCommand;
import outstar.user.command.OutStarMemberCommand;
import outstar.user.command.OutStarMemberDeletCheckCommand;
import outstar.user.command.OutStarMemberUpdateCommand;
import outstar.user.command.OutStarMyMainCommand;
import outstar.user.command.OutStarPassUpdateCommand;
import outstar.util.ActionForward;

/**
 * Servlet implementation class UserFrontController
 */
@WebServlet("*.do")
public class UserFrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserFrontController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uri = request.getRequestURI();
		String path = request.getContextPath();
		String what = uri.substring(path.length());
		
		OutStarCommand com = null;
		ActionForward af = null;
		
		System.out.println("what : " + what);
		
		if(what.equalsIgnoreCase("/jsp/user/join.do")){
			com = new OutStarJoinCommand();
		}else if(what.equalsIgnoreCase("/jsp/user/checkid.do")){
			com = new OutStarCheckId();
		}else if(what.equalsIgnoreCase("/jsp/user/checkName.do")){
			com = new OutStarCheckName();
		}else if(what.equalsIgnoreCase("/jsp/user/login.do")){
			com = new OutStarLoginCommand();
		}else if(what.equalsIgnoreCase("/jsp/page/myMain.do") 
				|| what.equalsIgnoreCase("/jsp/user/myMain.do")){
			com = new OutStarMyMainCommand();
		}else if(what.equalsIgnoreCase("/jsp/user/member.do")){
			com = new OutStarMemberCommand();
		}else if(what.equalsIgnoreCase("/jsp/user/passUpdate.do")){
			com = new OutStarPassUpdateCommand();
		}else if(what.equalsIgnoreCase("/jsp/user/memberUpdate.do")){
			com = new OutStarMemberUpdateCommand();
		}else if(what.equalsIgnoreCase("/jsp/user/deletMember.do")){
			com = new OutStarDeletMemberCommand();
		}else if(what.equalsIgnoreCase("/jsp/user/memberDeletCheck.do")){
			com = new OutStarMemberDeletCheckCommand();
		}else if(what.equalsIgnoreCase("/jsp/user/logout.do")){
			com = new OutStarLogoutCommand();
		}
		
		if(com != null) af = com.execute(request, response);
		
		if(af == null) response.sendRedirect("../user/login.jsp");
		
		if(af.isRedirect()){
			response.sendRedirect(af.getPath());
		}else{
			RequestDispatcher dis = request.getRequestDispatcher(af.getPath());
			dis.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);

		/* - img���� �׽�Ʈ��
		String uri = request.getRequestURI();
		String path = request.getContextPath();
		String what = uri.substring(path.length());
		
		ActionForward af = null;
		
		System.out.println("what : " + what);
		
		if(what.equalsIgnoreCase("/jsp/img.do")){
			System.out.println("imgUpLoad : " + new UtilImg().imgUpLoad(request, response));
		}else if(what.equalsIgnoreCase("/jsp/imgDelete.do")){
			System.out.println("imgDelete : " + new UtilImg().imgDelete(request.getParameter("pIdx")));
		}else if(what.equalsIgnoreCase("/jsp/imgRead.do")){
			request.setAttribute("list", new UtilImg().imgRead(request.getParameter("pIdx")));
		}
		
		af = new ActionForward(false, "imgTest.jsp");
		
			RequestDispatcher dis = request.getRequestDispatcher(af.getPath());
			dis.forward(request, response);
		
	//*/
	}
}
