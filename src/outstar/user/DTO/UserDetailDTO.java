package outstar.user.DTO;

public class UserDetailDTO extends UserDTO{
	private String UserKey;
	private String UserName;
	private String UserMail;
	private int UserPhone;
	private String UserArea;
	private int UserGender;
	private String UserType;
	
	public UserDetailDTO() {}

	public UserDetailDTO(String userKey, String userName, String userMail, int userPhone, String userArea, int userGender,
			String userType) {
		super();
		UserKey = userKey;
		UserName = userName;
		UserMail = userMail;
		UserPhone = userPhone;
		UserArea = userArea;
		UserGender = userGender;
		UserType = userType;
	}

	@Override
	public String toString() {
		return "UserDetail [UserKey=" + UserKey + ", UserName=" + UserName + ", UserMail=" + UserMail + ", UserPhone="
				+ UserPhone + ", UserArea=" + UserArea + ", UserGender=" + UserGender + ", UserType=" + UserType + "]";
	}

	public String getUserKey() {
		return UserKey;
	}
	public void setUserKey(String userKey) {
		UserKey = userKey;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getUserMail() {
		return UserMail;
	}
	public void setUserMail(String userMail) {
		UserMail = userMail;
	}
	public int getUserPhone() {
		return UserPhone;
	}
	public void setUserPhone(int userPhone) {
		UserPhone = userPhone;
	}
	public String getUserArea() {
		return UserArea;
	}
	public void setUserArea(String userArea) {
		UserArea = userArea;
	}
	public int getUserGender() {
		return UserGender;
	}
	public void setUserGender(int userGender) {
		UserGender = userGender;
	}
	public String getUserType() {
		return UserType;
	}
	public void setUserType(String userType) {
		UserType = userType;
	}
}