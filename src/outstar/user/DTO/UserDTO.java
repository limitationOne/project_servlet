package outstar.user.DTO;

public class UserDTO {
	private String userID;
	private String password;
	private String userkey;
	
	public UserDTO() {
		// TODO Auto-generated constructor stub
	}

	public UserDTO(String userID, String password, String userkey) {
		super();
		this.userID = userID;
		this.password = password;
		this.userkey = userkey;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserkey() {
		return userkey;
	}

	public void setUserkey(String userkey) {
		this.userkey = userkey;
	}


	

}
