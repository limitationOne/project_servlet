package outstar.user.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import outstar.db.DBConnection;
import outstar.user.DTO.UserDetailDTO;
import outstar.util.Sha256;

// ȸ������ insert �κ�
public class OutStarUserDAO {
	
	public int join(String userID, String password, String userName) {
		int check = isCheck(userID,userName);
		
		if(check == 0){
			String insertSql = "insert into UserLogin (userID, password,userkey) values(?,?,to_char(UserLogin_seq.nextval))";
			String serchSql = "select userkey from userlogin where userid=? and password=?";
			String updateSql = "update userLogin set userkey=? where userid=? and password=?";
			String detailInsertSql = "insert into UserDetail (UserKey, UserName, UserType) values(?,?,0)";
			String userKey = null; 
			
			Connection conn = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {
				conn = DBConnection.getDb().getDataFactory().getConnection();
				conn.setAutoCommit(false);
				pstmt = conn.prepareStatement(insertSql);
				
				pstmt.setString(1, userID);
				pstmt.setString(2, password);
				pstmt.executeUpdate();
				
				if(pstmt != null) pstmt.close();
				
				// key 갖고오기
				pstmt = conn.prepareStatement(serchSql);
				
				pstmt.setString(1, userID);
				pstmt.setString(2, password);
				
				rs = pstmt.executeQuery();
				
				if(!rs.next()) return 3;
				
				// 암호화 후 저장
				userKey = Sha256.encrypt(String.valueOf(rs.getString(1)));
				
				if(pstmt != null) pstmt.close();
				if(rs != null) rs.close();
				
				pstmt = conn.prepareStatement(updateSql);
				
				pstmt.setString(1, userKey);
				pstmt.setString(2, userID);
				pstmt.setString(3, password);
				
				pstmt.executeUpdate();
				
			//-----------------------  �Ʒ��κ��� UserDetail ���̺� �ִ� ���� ---------------------------------
				if(pstmt != null) pstmt.close();
				
				pstmt = conn.prepareStatement(detailInsertSql);
				pstmt.setString(1, userKey);
				pstmt.setString(2, userName);
				pstmt.executeUpdate();
				
				conn.commit();
				
			} catch (Exception e) {
				e.printStackTrace();
				try {
					conn.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}finally{
				try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				CloseAll(rs, null, pstmt, conn);
			}
		}
		return check;
		
	}
	
	private int isCheck(String userID, String userName) {
	
		// idcheck �� ture �̸� ���̵� �ߺ��ǿ���
		boolean idCheck = checkId(userID);
		// nameCheck �� ture �̸� �г��� �ߺ��ǿ���
		boolean nameCheck = ckeckName(userName);
		
		int check = 0;
		
		if(idCheck){
			check = 1;
		}else if(nameCheck){
			check = 2;
		}
		return check;
	}

	// id �ߺ�üũ
	public boolean checkId(String userId) {
		Connection conn=null;
		PreparedStatement pstmt= null;
		ResultSet rs = null;
		
		boolean check= true;
		String sql ="select * from UserLogin where UserID=?";
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt =conn.prepareStatement(sql);
			pstmt.setString(1, userId);
			rs = pstmt.executeQuery();
			if(!rs.next()){
				check = false;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(rs, null, pstmt, conn);
		}
		return check;
	}
	// pass üũ
	public boolean passCheck(String userKey, String oldPass) {
		String sql = "select *from userLogin where userKey=?";
		boolean passCheck = true;
		String pass = null;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, userKey);
			
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				pass = rs.getString("password");
			}
			if(pass.equals(oldPass)){
				passCheck = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(rs, null, pstmt, conn);
		}
		return passCheck;
	}
	
	// Name üũ
	public boolean ckeckName(String userName) {
		Connection conn =null;
		PreparedStatement pstmt = null;
		ResultSet rs= null;
		
		boolean check= true;
		String sql = "select *from UserDetail where UserName=? ";
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt= conn.prepareStatement(sql);
			pstmt.setString(1, userName);
			rs = pstmt.executeQuery();
			
			if(!rs.next()){
				check = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(rs, null, pstmt, conn);
		}
		return check;
	}

	// userKey ���� �κ�
	private int LogCount() {
		String sql = "select count(*) from UserLogin";
		int userKey = 0;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			rs =pstmt.executeQuery();
			
			while(rs.next()){
				userKey = rs.getInt(1)+1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(rs, null, pstmt, conn);
		}
		return userKey;
	}

	// �α��� �κ� �� ���̵� �н����� �������� üũ 
	public int loginCheck(String userId, String password) {
		int isOk = 1; //���̵� �������� ����.
		String pass = null;
		String id = null;
		String sql = "select * from UserLogin where userId=?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,userId);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				pass = rs.getString("Password");
				id = rs.getString("userId");
			}
			
			if( pass != null && !pass.equals(password) ){
				isOk = 2;//password ��Ʋ��
			}else if(id.equals(userId) && pass.equals(password) ) {
				isOk =3;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(rs, null, pstmt, conn);
		}
		return isOk;
	}
	
	//userkey ��� 
	public String userKey(String userId) {
		Connection conn = null;
		PreparedStatement pstmt= null;
		ResultSet rs = null;
		
		String userKey = null;
		String sql = "select * from UserLogin where userId=?";
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userId);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				userKey = rs.getString("userKey");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(rs, null, pstmt, conn);
		}
		return userKey;
	}
	
	//userName ��� �κ�
	public String userName(int userKey) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		String userName = null;
		String sql ="select *from userDatail where userkey=?";
		
		try {
			conn =DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1,userKey);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				userName = rs.getString("userName");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(rs, null, pstmt, conn);
		}
		return userName;
	}
	
	/*
	public UserDetailDTO login(String userId, String password) {
		System.out.println("login()");
		
		if(userId == null || "".equals(userId.replaceAll(" ", "")) 
				|| password == null || "".equals(password.replaceAll(" ", "")) ) {
			return null;
		}
		UserDetailDTO dto = new UserDetailDTO();
		
		String loginSql = "select * from Userlogin where userId=? and password=?";
		String detailSql = "select * from UserDetail where userKey=?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(loginSql);
			
			pstmt.setString(1, userId);
			pstmt.setString(2, password);
			
			rs = pstmt.executeQuery();
			
			if(!rs.next()) return null;
			
			String key = rs.getString(1);
			
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			
			pstmt = conn.prepareStatement(detailSql);
			
			pstmt.setString(1, key);
			
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				dto.setUserKey(key);
				dto.setUserName(rs.getString("UserName"));
				dto.setUserType(rs.getString("UserType"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(rs, null, pstmt, conn);
		}
		return dto;
	}
	//*/
	
	////UserDetailDTO ��ȯ �� ȸ������ ����
	public UserDetailDTO login(String userId, String password) {
		Connection conn =null;
		PreparedStatement pstmt= null;
		ResultSet rs = null;
		UserDetailDTO dto = new UserDetailDTO();
		String sql = "select *from UserLogin where UserId=? and password=?";
		String userKey = null;
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userId);
			pstmt.setString(2,password);
			rs =pstmt.executeQuery();
			if(rs.next()){
				userKey = rs.getString("userKey");
			}
			
			pstmt.close();
			rs.close();
			
			////// userDetail/////////
			String sql2 ="select *from userDetail where userKey=?";
			pstmt = conn.prepareStatement(sql2);
			pstmt.setString(1, userKey);
			rs = pstmt.executeQuery();
			if(rs.next()){
				dto = new UserDetailDTO(
						rs.getString("userKey"),
						rs.getString("userName"),
						rs.getString("userMail"),
						rs.getInt("userPhone"),
						rs.getString("userArea"),
						rs.getInt("userGender"),
						rs.getString("userType")
						);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(rs, null, pstmt, conn);
		}
		return dto;
	}
	
	// member��ȸ �� �ڽ��� ������ �����ִ� �κ�
	public UserDetailDTO member(int userKey) {
		UserDetailDTO dto = new UserDetailDTO();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select *from userDetail where userkey=?";
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userKey);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				dto = new UserDetailDTO(
						rs.getString("userKey"),
						rs.getString("userName"),
						rs.getString("userMail"),
						rs.getInt("userPhone"),
						rs.getString("userArea"),
						rs.getInt("userGender"),
						rs.getString("userType")
						);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(rs, null, pstmt, conn);
		}
		return dto;
	}
	
	// member passwordUpdate �����κ�
	public void passUpdate(int userKey, String newPass1) {
		Connection conn =null;
		PreparedStatement pstmt =null;
		String sql = "update userLogin set passWord=? where userKey=?";
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,newPass1 );
			pstmt.setInt(2, userKey);
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(null, null, pstmt, conn);
		}
	}
	
	// member ���� �κ�
	public void memberUpdate(String userKey, String userMail, String userArea, String suserPhone) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "update UserDetail set userMail=?, userArea=?, suserPhone=? where userKey=?";
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userMail);
			pstmt.setString(2, userArea);
			pstmt.setString(3, suserPhone);
			pstmt.setString(4, userKey);
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(null, null, pstmt, conn);
		}
	}
	
	// delet �Ҷ� ���̵�� ��� üũ Ȯ���ϴ°� 
	public boolean isOk(String userId, String userPassword) {
		boolean isOk = false;
		int is = loginCheck(userId,userPassword);
		if(is == 3){
			isOk = true;
		}
		return isOk;
	}
	
	// member��ȸ �� �ڽ��� ������ �����ִ� �κ�
	public UserDetailDTO member(String userKey) {
		
		UserDetailDTO dto = new UserDetailDTO();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select * from userDetail where userkey=?";
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userKey);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				dto = new UserDetailDTO(
						rs.getString("userKey"),
						rs.getString("userName"),
						rs.getString("userMail"),
						rs.getInt("userPhone"),
						rs.getString("userArea"),
						rs.getInt("userGender"),
						rs.getString("userType")
						);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(rs, null, pstmt, conn);
		}
		return dto;
	}
	
	// member passwordUpdate �����κ�
	public void passUpdate(String userKey, String newPass1) {
		System.out.println("passUpdate ���ƴ�");
		Connection conn =null;
		PreparedStatement pstmt =null;
		String sql = "update userLogin set passWord=? where userKey=?";
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,newPass1 );
			pstmt.setString(2, userKey);
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(null, null, pstmt, conn);
		}
	}
	
	// member ���� �κ�
	public void memberUpdate(String userKey, String userMail, String userArea, int userPhone) {
		System.out.println("memberUpdate()");
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "update UserDetail set userMail=?, userArea=?, userPhone=? where userKey=?";
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userMail);
			pstmt.setString(2, userArea);
			pstmt.setInt(3, userPhone);
			pstmt.setString(4, userKey);
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			CloseAll(null, null, pstmt, conn);
		}
	}
	
	//memberdelet  �κ�
	public void deletMember(String userKey) {
		// TODO Auto-generated method stub
		System.out.println("deletMember");
		
		Connection conn =null;
		PreparedStatement pstmt =null;
		String sql1 = "delete from userDetail where userKey=?";
		String sql2 = "delete from userLogin where userKey=?";
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(sql1);
			pstmt.setString(1, userKey);
			pstmt.executeQuery();
			
			pstmt.close();
			
			pstmt = conn.prepareStatement(sql2);
			pstmt.setString(1, userKey);
			pstmt.executeQuery();
			conn.commit();
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (Exception e2) {
				// TODO: handle exception
				e2.printStackTrace();
			}
		}finally{
			CloseAll(null, null, pstmt, conn);
		}
		
	}

	// close�κ�
	public void CloseAll(ResultSet rs, CallableStatement cstmt, PreparedStatement pstmt, Connection conn){
		try {
			if(rs != null) rs.close();
			if(cstmt != null) cstmt.close();
			if(pstmt != null) pstmt.close();
			if(conn != null) conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
