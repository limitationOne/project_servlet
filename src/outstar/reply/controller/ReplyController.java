package outstar.reply.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.reply.command.ReplyCommand;
import outstar.reply.command.ReplyDeleteCommand;
import outstar.reply.command.ReplyListCommand;
import outstar.reply.command.ReplyMyListCommand;
import outstar.reply.command.ReplyWriteCommand;
import outstar.util.ActionForward;
import outstar.util.UtilAjax;

/**
 * Servlet implementation class ReplyController
 */
@WebServlet("*.re")
public class ReplyController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReplyController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String path = request.getServletPath();
		ReplyCommand com = null;
		
		System.out.println(path);
		
		if("/jsp/reply/replyWrite.re".equalsIgnoreCase(path)){
			com = new ReplyWriteCommand();
		}else if("/jsp/reply/replyList.re".equalsIgnoreCase(path)){
			com = new ReplyListCommand();
		}else if("/jsp/reply/myReplyList.re".equalsIgnoreCase(path)){
			com = new ReplyMyListCommand();
		}else if("/jsp/reply/replyDelete.re".equalsIgnoreCase(path)){
			com = new ReplyDeleteCommand();
		}
		
		if(com == null) return;
		
		Object data = com.execute(request, response);
		
		try {
			if((int)data == -1) new ActionForward(false, "../user/login.jsp");
		} catch (ClassCastException e) {}
		
		new UtilAjax(data, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}