package outstar.reply.command;

import java.io.IOError;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ReplyCommand {
	public Object execute(HttpServletRequest request, HttpServletResponse response) throws IOError, ServletException;
}