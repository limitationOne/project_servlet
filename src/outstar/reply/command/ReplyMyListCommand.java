package outstar.reply.command;

import java.io.IOError;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outstar.reply.DAO.ReplyDAO;

public class ReplyMyListCommand implements ReplyCommand {

	@Override
	public Object execute(HttpServletRequest request, HttpServletResponse response) 
			throws IOError, ServletException {
		System.out.println("ReplyMyListCommand");
		
		HttpSession session = request.getSession();
		
		if(session == null) return null;
		
		String userKey = (String) session.getAttribute("userKey");
		
		if(userKey == null || "".equals(userKey)) return null;
		
		return new ReplyDAO().replyMyList(userKey);
	}
}