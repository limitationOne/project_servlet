package outstar.reply.command;

import java.io.IOError;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outstar.reply.DAO.ReplyDAO;

public class ReplyWriteCommand implements ReplyCommand {

	@Override
	public Object execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServletException {
		
		System.out.println("ReplyWriteCommand");
		
		HttpSession session = request.getSession();
		
		if(session == null) return -1;
		
		String userKey = (String) session.getAttribute("userKey");
		String userName = (String) session.getAttribute("userName");
		String replyContent = request.getParameter("replyContent");
		String postIdx = request.getParameter("postIdx");
		
		String replyIdxStr = request.getParameter("replyIdx");
		String replyRootStr = request.getParameter("replyRoot");
		String replyStepStr = request.getParameter("replyStep");
		String replyIndentStr = request.getParameter("replyIndent");
		
		if( userKey == null || "".equals(userKey) 
				|| userName == null || "".equals(userName) 
				|| postIdx == null	|| "".equals(postIdx) 
				|| replyContent == null || "".equals(replyContent)) {
			return 0;
		}
		int replyIdx = 0;
		int replyRoot = 0;
		int replyStep = 0;
		int replyIndent = 0;
		
		int check = 0;
		
		if(replyIdxStr != null && !"".equals(replyIdxStr)) replyIdx = Integer.parseInt(replyIdxStr);
		if(replyRootStr != null && !"".equals(replyRootStr)) replyRoot = Integer.parseInt(replyRootStr);
		if(replyStepStr != null && !"".equals(replyStepStr)) replyStep = Integer.parseInt(replyStepStr);
		if(replyIndentStr != null && !"".equals(replyIndentStr)) replyIndent = Integer.parseInt(replyIndentStr);
		
		check = new ReplyDAO().replyWrite(
				Integer.parseInt(postIdx), 
				userKey, userName, replyContent, 
				replyIdx, replyIndent, replyRoot, replyStep);
		
		return check;
	}
}