package outstar.reply.command;

import java.io.IOError;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.reply.DAO.ReplyDAO;

public class ReplyListCommand implements ReplyCommand {
	@Override
	public Object execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServletException {
		
		System.out.println("ReplyListCommand");
		
		String postIdx = request.getParameter("postIdx");
		
		return new ReplyDAO().replyList(Integer.parseInt(postIdx));
	}
}