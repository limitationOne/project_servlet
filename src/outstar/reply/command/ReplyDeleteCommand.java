package outstar.reply.command;

import java.io.IOError;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outstar.reply.DAO.ReplyDAO;

public class ReplyDeleteCommand implements ReplyCommand {

	@Override
	public Object execute(HttpServletRequest request, HttpServletResponse response) throws IOError, ServletException {
		
		HttpSession session = request.getSession();
		
		if(session == null) return -1;
		
		String userKey = (String) session.getAttribute("userKey");
		String replyIdxStr = request.getParameter("replyIdx");
		
		if(userKey == null || "".equals(userKey) || replyIdxStr == null || "".equals(replyIdxStr)) return -1;
		
		return new ReplyDAO().replyDelete(userKey, Integer.parseInt(replyIdxStr));
	}
}