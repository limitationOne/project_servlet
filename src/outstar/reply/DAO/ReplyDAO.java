package outstar.reply.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import outstar.db.DBConnection;
import outstar.reply.DTO.ReplyDTO;

public class ReplyDAO {
	
	// 첫 댓글의 순서는 작성순으로 정렬되지만, 댓글에 달린 댓글의 경우는 최신순으로 정렬됨.
	// 댓글의 댓글이 작성순으로 정렬되게 하기 위해서는 다른 방식을 알아보는것이 좋을것 같음.
	// 아니면 댓글에 댓글은 1회만 가능하도록 하거나..
	
	// 리플 작성
	public int replyWrite(int postIdx, String userKey, String userName,
			String replyContent, int replyIdx, int replyIndent, int replyRoot, int replyStep) {
		System.out.println("replyWrite()");
		
		if(replyRoot == 0) replyRoot = replyIdx;
		
		addReplyStep(replyRoot, replyStep);
		
		if(replyIdx != 0){
			replyIndent += 1;
			replyStep += 1;
		}
		
		
		int check = 0;
		// 원래글과 댓글을 붙어있게 하기 위한 정보, 댓글에 대한 순서 지정을 위한 컬럼, 들여쓰기정보
		String insertSql = "insert into reply "
				+ "(replyIdx, replyPostIdx, UserKey, replyUserName, "
				+ "replyContent, replyRoot, replyStep, replyIndent) "
				+ "values (reply_seq.nextval, ?,?,?, ?,?,?,?)";
		
		String updateRIdxSql = "update (select * from reply where userKey=?) set replyRoot=replyIdx";
		
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(insertSql);
			
			pstmt.setInt(1, postIdx);
			pstmt.setString(2, userKey);
			pstmt.setString(3, userName);
			pstmt.setString(4, replyContent);
			pstmt.setInt(5, replyRoot);
			pstmt.setInt(6, replyStep);
			pstmt.setInt(7, replyIndent);

			check = pstmt.executeUpdate();
			
			if(replyIdx != 0){
				conn.commit();
				return check;
			}
			if(pstmt != null) pstmt.close();
			pstmt = conn.prepareStatement(updateRIdxSql);
			
			pstmt.setString(1, userKey);
			
			pstmt.executeUpdate();
			
			conn.commit();
			
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			cloaseAll(null, null, pstmt, conn);
		}
		return check;
	}
	
	// 리플 리스트
	public ArrayList<ReplyDTO> replyList(int postIdx){
		System.out.println("replyList()");
		
		JSONArray list = new JSONArray();
		String sql = "select * from reply where replyPostIdx=? order by replyRoot asc, replyStep asc";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);

			pstmt.setInt(1, postIdx);

			rs = pstmt.executeQuery();
			
			while(rs.next()){
				JSONObject jsonObj = new JSONObject();
				
				jsonObj.put("replyIndent", rs.getInt("replyIndent"));
				jsonObj.put("replyStep", rs.getInt("replyStep"));
				jsonObj.put("replyRoot", rs.getInt("replyRoot"));
				jsonObj.put("replyLike", rs.getInt("replyLike"));
				jsonObj.put("replyDate", rs.getString("replyDate"));
				jsonObj.put("replyContent", rs.getString("replyContent"));
				jsonObj.put("replyUserName", rs.getString("replyUserName"));
				jsonObj.put("userKey", rs.getString("userKey"));
				jsonObj.put("replyPostIdx", rs.getInt("replyPostIdx"));
				jsonObj.put("replyIdx", rs.getInt("replyIdx"));
				
				list.add(jsonObj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cloaseAll(rs, null, pstmt, conn);
		}
		return list;
	}
	
	// 특정유저 리플 리스트
	public ArrayList<ReplyDTO> replyMyList(String userKey){
		System.out.println("replyMyList()");
		
		JSONArray list = new JSONArray();
		String sql = "select * from reply where userKey=? order by replyIdx desc";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, userKey);
			
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				JSONObject jsonObj = new JSONObject();
				
				jsonObj.put("replyIndent", rs.getInt("replyIndent"));
				jsonObj.put("replyStep", rs.getInt("replyStep"));
				jsonObj.put("replyRoot", rs.getInt("replyRoot"));
				jsonObj.put("replyLike", rs.getInt("replyLike"));
				jsonObj.put("replyDate", rs.getString("replyDate"));
				jsonObj.put("replyContent", rs.getString("replyContent"));
				jsonObj.put("replyUserName", rs.getString("replyUserName"));
				jsonObj.put("userKey", rs.getString("userKey"));
				jsonObj.put("replyPostIdx", rs.getInt("replyPostIdx"));
				jsonObj.put("replyIdx", rs.getInt("replyIdx"));
				
				list.add(jsonObj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cloaseAll(rs, null, pstmt, conn);
		}
		return list;
	}
	
	// 리플에 리플 순서
	private void addReplyStep(int replyRoot, int replyStep){
		String sql = "update reply set replyStep=replyStep+1 where replyRoot = ? and ? < replyStep";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, replyRoot);
			pstmt.setInt(2, replyStep);
			
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cloaseAll(null, null, pstmt, conn);
		}
	}
	
	// 리플 삭제
	public int replyDelete(String userKey, int replyIdxStr){
		System.out.println("replyDelete()");
		
		String sql = "delete from reply where userkey=? and replyIdx=?";
		int check = 0;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, userKey);
			pstmt.setInt(2, replyIdxStr);
			
			check = pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cloaseAll(null, null, pstmt, conn);
		}
		return check;
	}
	
	private void cloaseAll(ResultSet rs, CallableStatement cstmt,
			PreparedStatement pstmt, Connection conn) {
		try {
			if (rs != null) rs.close();
			if (cstmt != null) cstmt.close();
			if (pstmt != null) pstmt.close();
			if (conn != null) conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}