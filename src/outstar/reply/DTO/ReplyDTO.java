package outstar.reply.DTO;

public class ReplyDTO {
	private int replyIdx;
	private int replyPostIdx;
	
	private String UserKey;
	private String replyUserName;
	private String replyContent;
	private String replyDate;
	
	private int replyLike;
	private int replyIndent;
	private int replyRoot;
	private int replyStep;
	
	public ReplyDTO() {}

	public ReplyDTO(int replyIdx, int replyPostIdx, String userKey,
			String replyUserName, String replyContent, String replyDate,
			int replyLike, int replyIndent, int replyRoot, int replyStep) {
		super();
		this.replyIdx = replyIdx;
		this.replyPostIdx = replyPostIdx;
		this.UserKey = userKey;
		this.replyUserName = replyUserName;
		this.replyContent = replyContent;
		this.replyDate = replyDate;
		this.replyLike = replyLike;
		this.replyIndent = replyIndent;
		this.replyRoot = replyRoot;
		this.replyStep = replyStep;
	}
	
	@Override
	public String toString() {
		return "ReplyDTO [replyIdx=" + replyIdx + ", replyPostIdx="
				+ replyPostIdx + ", UserKey=" + UserKey + ", replyUserName="
				+ replyUserName + ", replyContent=" + replyContent
				+ ", replyDate=" + replyDate + ", replyLike=" + replyLike
				+ ", replyIndent=" + replyIndent + ", replyRoot=" + replyRoot
				+ ", replyStep=" + replyStep + "]";
	}

	public int getReplyIdx() {
		return replyIdx;
	}
	public void setReplyIdx(int replyIdx) {
		this.replyIdx = replyIdx;
	}
	public int getReplyPostIdx() {
		return replyPostIdx;
	}
	public void setReplyPostIdx(int replyPostIdx) {
		this.replyPostIdx = replyPostIdx;
	}
	public String getUserKey() {
		return UserKey;
	}
	public void setUserKey(String userKey) {
		this.UserKey = userKey;
	}
	public String getReplyUserName() {
		return replyUserName;
	}
	public void setReplyUserName(String replyUserName) {
		this.replyUserName = replyUserName;
	}
	public String getReplyContent() {
		return replyContent;
	}
	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}
	public String getReplyDate() {
		return replyDate;
	}
	public void setReplyDate(String replyDate) {
		this.replyDate = replyDate;
	}
	public int getReplyLike() {
		return replyLike;
	}
	public void setReplyLike(int replyLike) {
		this.replyLike = replyLike;
	}
	public int getReplyIndent() {
		return replyIndent;
	}
	public void setReplyIndent(int replyIndent) {
		this.replyIndent = replyIndent;
	}
	public int getReplyRoot() {
		return replyRoot;
	}
	public void setReplyRoot(int replyRoot) {
		this.replyRoot = replyRoot;
	}
	public int getReplyStep() {
		return replyStep;
	}
	public void setReplyStep(int replyStep) {
		this.replyStep = replyStep;
	}
}