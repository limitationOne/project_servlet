package outstar.posts.ajax;

import java.io.IOError;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outstar.posts.DAO.PostDAO;
import outstar.util.UtilAjax;

public class PostAddLikeCommand {

	public void execute(HttpServletRequest request,
			HttpServletResponse response) throws IOError, ServletException {
		
		HttpSession session = request.getSession(false);
		
		if(session == null) {
			new UtilAjax("-1", response);
			return;
		}
		String key = (String) session.getAttribute("userKey");
		String pIdx = request.getParameter("postIdx");
		
		int check = new PostDAO().postAddLike(key, pIdx);
		
		new UtilAjax(Integer.toString(check), response);
	}
}