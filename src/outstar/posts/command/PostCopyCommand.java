package outstar.posts.command;

import java.io.IOError;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outstar.posts.DAO.PostDAO;
import outstar.posts.DTO.PostDTO;
import outstar.util.ActionForward;

public class PostCopyCommand implements PostCommand {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws IOError, ServletException {
		
		ActionForward af = new ActionForward(false, "../user/login.jsp");
		
		HttpSession session = request.getSession(false);
		
		if(session == null ) return af;
		
		String userKey = (String) session.getAttribute("userKey");
		String userName = (String) session.getAttribute("userName");
		String postIdx = request.getParameter("postIdx");
		
		if( userKey == null || "".equals(userKey) 
				|| userName == null || "".equals(userName) 
				|| postIdx == null || "".equals(postIdx)) {
			session.invalidate();
			return af;
		}
		ArrayList<PostDTO> postList = new PostDAO().postCopy(userKey, userName, Integer.parseInt(postIdx));
		
		if(postList == null || postList.size() < 1) return af;
		
		request.setAttribute("postList", postList);
		
		af.setPath("../page/myMain.jsp");
		
		return af;
	}
}