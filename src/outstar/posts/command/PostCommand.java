package outstar.posts.command;

import java.io.IOError;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.util.ActionForward;

public interface PostCommand {
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) 
			throws IOError, ServletException;
}
