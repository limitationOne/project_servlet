package outstar.posts.command;

import java.io.IOError;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import outstar.posts.DAO.PostDAO;
import outstar.util.ActionForward;
import outstar.util.UtilImg;

public class PostWriteCommand implements PostCommand {
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) 
			throws IOError, ServletException {
		
		ActionForward af = new ActionForward(false, "../user/login.jsp");
		String postContent = "";
		int imgCheck = 0;
		
		DiskFileItemFactory factory = null;
		ServletFileUpload upLoad = null;
		ArrayList<FileItem> items = null;
		
		HttpSession session = request.getSession(false);
		
		if(session == null) return af;
		
		try {
			factory = new DiskFileItemFactory();
			upLoad = new ServletFileUpload(factory);
			items = (ArrayList<FileItem>) upLoad.parseRequest(new ServletRequestContext(request));
			
			Iterator<FileItem> iter = items.iterator();
			
			// ���� Ȯ��
			while (iter.hasNext()) {
			    FileItem item = (FileItem) iter.next();
			    
			    if(item.getName() != null && !"".equals(item.getName())) {
			    	imgCheck = 1;
			    }
			    if (item.isFormField()) {
			    	postContent = new String(item.get()).replaceAll("\n", "<br/>");
			    }
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
		
		String key = (String)session.getAttribute("userKey");
		String name = (String)session.getAttribute("userName");
		
		if(key == null || "".equals(key) || name == null || "".equals(name)) return af;
		
		int postIdx = new PostDAO().postWrite(key, name, postContent, imgCheck);
		
		if(imgCheck == 1){
			new UtilImg().imgUpLoad(factory, items, Integer.toString(postIdx));
		}
		
		request.setAttribute("postList", new PostDAO().postGet(postIdx));
		
		af.setPath("../page/myMain.jsp");
		
		return af;
	}
}