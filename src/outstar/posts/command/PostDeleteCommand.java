package outstar.posts.command;

import java.io.IOError;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.posts.DAO.PostDAO;
import outstar.util.ActionForward;

public class PostDeleteCommand implements PostCommand {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServletException {
		ActionForward af = new ActionForward(false, "../page/myMain.do");
		
		String postIdx = request.getParameter("postIdx");
		
		new PostDAO().postDelete(Integer.parseInt(postIdx));
		
		return af;
	}
}