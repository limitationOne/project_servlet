package outstar.posts.command;

import java.io.IOError;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import outstar.posts.DAO.PostDAO;
import outstar.posts.DTO.PostDTO;
import outstar.util.ActionForward;

public class PostListCommand implements PostCommand {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws IOError, ServletException {
		ActionForward af = new ActionForward(false, "../user/login.jsp");
		
		HttpSession session = request.getSession(false);
		
		if(session == null ) return af;
		
		String key = (String)session.getAttribute("userKey");
		
		ArrayList<PostDTO> list = new PostDAO().postList(key);
		
		if(list != null && list.size() > 0){
			request.setAttribute("postList", list);
		}
		af.setPath("../page/myMain.jsp");
		
		return af;
	}
}