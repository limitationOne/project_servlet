package outstar.posts.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import outstar.db.DBConnection;
import outstar.posts.DTO.PostDTO;
import outstar.util.UtilImg;
import outstar.util.DTO.ImgDTO;


public class PostDAO {
	
	// ����Ʈ �ۼ�
	public int postWrite(String userKey, String userName, String content, int imgCheck){
		System.out.println("postWrite()");
		int postIdx = 0;
		
		if(userKey == null || "".equals(userKey)) return postIdx;
		if(imgCheck == 0 && (content == null || "".equals(content))) return postIdx;
		
		String sql = "insert into Posts (postIdx,UserKey,postUserName,postContent,postLike,postImgCheck) "
				+ "values (seq_postIdx.nextval,?,?,?,0,?)";
		
		String sql1 = "select * from (select postIdx from posts order by postIdx desc) where rownum <=1";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, userKey);
			pstmt.setString(2, userName);
			pstmt.setString(3, content);
			pstmt.setInt(4, imgCheck);
			
			pstmt.executeUpdate();
			
			if(pstmt != null) pstmt.close();
			
			pstmt = conn.prepareStatement(sql1);
			
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				postIdx = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cloaseAll(rs, null, pstmt, conn);
		}
		return postIdx;
	}
	
	// �Խñ� ����Ʈ
	public ArrayList<PostDTO> postList(){
		ArrayList<PostDTO> list = new ArrayList<PostDTO>();
		
		// ���� + �ȷο��ѻ���� ���� �ҷ�������
		
		System.out.println("postList()");
		
		String sql = "select * from posts order by postIdx desc";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			rs = pstmt.executeQuery();
			
			list = listSetRs(list, rs);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cloaseAll(rs, null, pstmt, conn);
		}
		return list;
	}
	
	// �Խñ� ����Ʈ
	public ArrayList<PostDTO> postList(String key){
		ArrayList<PostDTO> list = new ArrayList<PostDTO>();
		
		String sql = "select * from posts where UserKey=? order by postIdx desc";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, key);
			
			rs = pstmt.executeQuery();
			
			list = listSetRs(list, rs);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cloaseAll(rs, null, pstmt, conn);
		}
		return list;
	}
	
	// �Խñ�
	public ArrayList<PostDTO> postGet(int postIdx){
		ArrayList<PostDTO> list = new ArrayList<PostDTO>();
		
		String sql = "select * from posts where postIdx=?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, postIdx);
			
			rs = pstmt.executeQuery();
			
			list = listSetRs(list, rs);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cloaseAll(rs, null, pstmt, conn);
		}
		return list;
	}
	
	// �Խñ� ���� UI
	public ArrayList<PostDTO> postUpdateUI(int postIdx, String key) {
		System.out.println("postUpdateUI()");
		
		String sql = "select postIdx,postContent from posts where postIdx=? and userKey=?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ArrayList<PostDTO> list = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, postIdx);
			pstmt.setString(2, key);
			
			int check = pstmt.executeUpdate();
			
			if(check == 1) list = postGet(postIdx);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cloaseAll(null, null, pstmt, conn);
		}
		return list;
	}
	
	// �Խñ� ����
	public ArrayList<PostDTO> postUpdate(String content, int postIdx, String key) {
		System.out.println("postUpdate()");
		
		String sql = "update posts set postContent=? where postIdx=? and userKey=?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ArrayList<PostDTO> list = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, content);
			pstmt.setInt(2, postIdx);
			pstmt.setString(3, key);
			
			int check = pstmt.executeUpdate();
			
			if(check == 1) list = postGet(postIdx);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cloaseAll(null, null, pstmt, conn);
		}
		return list;
	}
	
	// �Խñ� ����
	public void postDelete(int postIdx){
		System.out.println("postDelete()");
		
		String sql = "delete from posts where postIdx=?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		try {
			new UtilImg().imgDelete(Integer.toString(postIdx));
			
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, postIdx);
			
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cloaseAll(null, null, pstmt, conn);
		}
	}
	
	// �Խñ� ����
	public ArrayList<PostDTO> postCopy(String userKey, String userName, int postIdx){
		System.out.println("postCopy()");
		
		ArrayList<PostDTO> list = postGet(postIdx);
		
		String insertSql = "insert into Posts (postIdx,UserKey,postUserName,postContent,postLike,postOriIdx,postImgCheck) "
				+ "values (seq_postIdx.nextval,?,?,?,0,?,?)";
		String getPathSql = "select path from img where pIdx=?";
		String getPIdxSql = "select postIdx from (select postIdx from posts where userKey=? order by postIdx desc)";
		String insertImgSql = "insert into img values (?,?)";
		String sql1 = "select * from (select * from posts where userKey=? order by postIdx desc) where rownum<=1";
		
		System.out.println(list.get(0).toString());
		
		int postImgCheck = list.get(0).getPostImgCheck();
		String getPath = null;
		int getPIdx = 0;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(insertSql);
			
			pstmt.setString(1, userKey);
			pstmt.setString(2, userName);
			pstmt.setString(3, list.get(0).getPostContent());
			pstmt.setInt(4, postIdx);
			pstmt.setInt(5, postImgCheck);
			
			pstmt.executeUpdate();
			
			if(pstmt != null) pstmt.close();
			
			if(postImgCheck == 1){
				pstmt = conn.prepareStatement(getPathSql);
				
				pstmt.setInt(1, postIdx);
				
				rs = pstmt.executeQuery();
				
				if(rs.next()) getPath = rs.getString(1);
				if(pstmt != null) pstmt.close();
				if(rs != null) rs.close();
				
				pstmt = conn.prepareStatement(getPIdxSql);
				
				pstmt.setString(1, userKey);
				
				rs = pstmt.executeQuery();
				
				if(rs.next()) getPIdx = rs.getInt(1);
				if(pstmt != null) pstmt.close();
				if(rs != null) rs.close();
				
				pstmt = conn.prepareStatement(insertImgSql);
				
				pstmt.setString(1, getPath);
				pstmt.setInt(2, getPIdx);
				
				pstmt.executeUpdate();
				
				if(pstmt != null) pstmt.close();
				if(rs != null) rs.close();
			}
			list.clear();
			pstmt = conn.prepareStatement(sql1);
			
			pstmt.setString(1, userKey);
			
			rs = pstmt.executeQuery();
			
			conn.commit();
			
			list = listSetRs(list, rs);
			
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			cloaseAll(rs, null, pstmt, conn);
		}
		return list;
	}
	
	// �Խñ� ���ƿ�
	public int postAddLike(String userKey, String pIdx){
		System.out.println("postAddLike()");
		
		String sql = "update posts set postLike=postLike+? where postIdx=?";
		String likeListCheckSql = "select count(*) from likeList where userKey=? and pIdx=?";
		String likeListAddSql = "insert into likeList values (?,?)";
		String likeListDeleteSql = "delete from likeList where userKey=? and pIdx=?";
		int check = -1;
		int num = 1;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			conn.setAutoCommit(false);
			
			pstmt = conn.prepareStatement(likeListCheckSql);
			
			pstmt.setString(1, userKey);
			pstmt.setInt(2, Integer.parseInt(pIdx));
			
			rs = pstmt.executeQuery();
			
			rs.next();
			
			if(rs.getInt(1) == 0){
				if(pstmt != null) pstmt.close();
				pstmt = conn.prepareStatement(likeListAddSql);
				check = 1;
			}else{
				if(pstmt != null) pstmt.close();
				pstmt = conn.prepareStatement(likeListDeleteSql);
				check = 2;
				num = -1;
			}
			pstmt.setString(1, userKey);
			pstmt.setInt(2, Integer.parseInt(pIdx));
			
			pstmt.executeUpdate();
			
			if(pstmt != null) pstmt.close();
			
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, num);
			pstmt.setInt(2, Integer.parseInt(pIdx));
			
			pstmt.executeUpdate();
			
			conn.commit();
			
		} catch (Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			cloaseAll(rs, null, pstmt, conn);
		}
		return check;
	}
	
	private ArrayList<PostDTO> listSetRs(ArrayList<PostDTO> list, ResultSet rs) throws Exception{
		while(rs.next()){
			int imgCheck = rs.getInt("postImgCheck");
			
			if(imgCheck == 0){
				list.add( new PostDTO(
						rs.getInt("postIdx"), 
						rs.getString("userKey"), 
						rs.getString("postUserName"), 
						rs.getString("postContent"), 
						rs.getString("postDate"), 
						rs.getInt("postLike"), 
						rs.getInt("postOriIdx"),
						rs.getInt("postImgCheck")));
			}else if(imgCheck == 1){
				int pIdx = rs.getInt("postIdx");
				ArrayList<ImgDTO> imgList = new UtilImg().imgRead(Integer.toString(pIdx));
				
				list.add( new PostDTO(
						pIdx, 
						rs.getString("userKey"), 
						rs.getString("postUserName"), 
						rs.getString("postContent"), 
						rs.getString("postDate"), 
						rs.getInt("postLike"), 
						rs.getInt("postOriIdx"),
						rs.getInt("postImgCheck"),
						imgList));
			}
		}
		return list;
	}
	
	private void cloaseAll(ResultSet rs, CallableStatement cstmt, PreparedStatement pstmt, Connection conn){
		try {
			if(rs != null) rs.close();
			if(cstmt != null) cstmt.close();
			if(pstmt != null) pstmt.close();
			if(conn != null) conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}