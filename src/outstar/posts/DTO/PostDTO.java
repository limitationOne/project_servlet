package outstar.posts.DTO;

import java.util.ArrayList;

import outstar.util.DTO.ImgDTO;

public class PostDTO {
	private int postIdx;
	private String userKey;
	private String postUserName;
	private String postContent;
	private String postDate;
	private int postLike;
	private int postOriIdx;
	private int postImgCheck;
	private ArrayList<ImgDTO> imgList;
	
	public PostDTO() {}
	
	public PostDTO(int postIdx, String userKey, String postUserName,
			String postContent, String postDate, int postLike, int postOriIdx) {
		super();
		this.postIdx = postIdx;
		this.userKey = userKey;
		this.postUserName = postUserName;
		this.postContent = postContent;
		this.postDate = postDate;
		this.postLike = postLike;
		this.postOriIdx = postOriIdx;
	}

	public PostDTO(int postIdx, String userKey, String postUserName, String postContent,
			String postDate, int postLike, int postOriIdx, int postImgCheck) {
		super();
		this.postIdx = postIdx;
		this.userKey = userKey;
		this.postUserName = postUserName;
		this.postContent = postContent;
		this.postDate = postDate;
		this.postLike = postLike;
		this.postOriIdx = postOriIdx;
		this.postImgCheck = postImgCheck;
	}
	
	public PostDTO(int postIdx, String userKey, String postUserName,
			String postContent, String postDate, int postLike, int postOriIdx, int postImgCheck,
			ArrayList<ImgDTO> imgList) {
		super();
		this.postIdx = postIdx;
		this.userKey = userKey;
		this.postUserName = postUserName;
		this.postContent = postContent;
		this.postDate = postDate;
		this.postLike = postLike;
		this.postOriIdx = postOriIdx;
		this.postImgCheck = postImgCheck;
		this.imgList = imgList;
	}

	@Override
	public String toString() {
		return "PostDTO [postIdx=" + postIdx + ", userKey=" + userKey
				+ ", postUserName=" + postUserName + ", postContent="
				+ postContent + ", postDate=" + postDate + ", postLike="
				+ postLike + ", postOriIdx=" + postOriIdx + ", postImgCheck="
				+ postImgCheck + ", imgList=" + imgList + "]";
	}

	public int getPostIdx() {
		return postIdx;
	}
	public void setPostIdx(int postIdx) {
		this.postIdx = postIdx;
	}
	public String getUserKey() {
		return userKey;
	}
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}
	public String getPostUserName() {
		return postUserName;
	}
	public void setPostUserName(String postUserName) {
		this.postUserName = postUserName;
	}
	public String getPostContent() {
		return postContent;
	}
	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public int getPostLike() {
		return postLike;
	}
	public void setPostLike(int postLike) {
		this.postLike = postLike;
	}
	public int getPostOriIdx() {
		return postOriIdx;
	}
	public void setPostOriIdx(int postOriIdx) {
		this.postOriIdx = postOriIdx;
	}
	public int getPostImgCheck() {
		return postImgCheck;
	}
	public void setPostImgCheck(int postImgCheck) {
		this.postImgCheck = postImgCheck;
	}
	public ArrayList<ImgDTO> getImgList() {
		return imgList;
	}
	public void setImgList(ArrayList<ImgDTO> imgList) {
		this.imgList = imgList;
	}
}