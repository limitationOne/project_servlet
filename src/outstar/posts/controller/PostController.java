package outstar.posts.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import outstar.posts.ajax.PostAddLikeCommand;
import outstar.posts.command.PostCommand;
import outstar.posts.command.PostCopyCommand;
import outstar.posts.command.PostDeleteCommand;
import outstar.posts.command.PostListCommand;
import outstar.posts.command.PostOriginalCommand;
import outstar.posts.command.PostUpdateCommand;
import outstar.posts.command.PostUpdateUICommand;
import outstar.posts.command.PostWriteCommand;
import outstar.util.ActionForward;

/**
 * Servlet implementation class PostController
 */
@WebServlet("*.po")
public class PostController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		System.out.println(".po");
		
		String uri = request.getRequestURI();
		String path = request.getContextPath();
		String what = uri.substring(path.length());
		
		PostCommand com = null;
		ActionForward af = null;
		
		System.out.println("what : " + what);
		
		if(what.equalsIgnoreCase("/jsp/page/postWrite.po")){
			com = new PostWriteCommand();
		}else if(what.equalsIgnoreCase("/jsp/page/postList.po")){
			com = new PostListCommand();
		}else if(what.equalsIgnoreCase("/jsp/page/postDelete.po")){
			com = new PostDeleteCommand();
		}else if(what.equalsIgnoreCase("/jsp/page/postUpdateUI.po")){
			com = new PostUpdateUICommand();
		}else if(what.equalsIgnoreCase("/jsp/page/postUpdate.po")){
			com = new PostUpdateCommand();
		}else if(what.equalsIgnoreCase("/jsp/page/postCopy.po")){
			com = new PostCopyCommand();
		}else if(what.equalsIgnoreCase("/jsp/page/postOriginal.po")){
			com = new PostOriginalCommand();
		}else if(what.equalsIgnoreCase("/jsp/page/postAddLike.po")){
			new PostAddLikeCommand().execute(request, response);
			return;
		}
		
		if(com != null) af = com.execute(request, response);
		
		if(af == null) return;
		
		if(af.isRedirect()){
			response.sendRedirect(af.getPath());
		}else{
			request.getRequestDispatcher(af.getPath()).forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}