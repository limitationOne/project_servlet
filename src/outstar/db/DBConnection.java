package outstar.db;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DBConnection {
	private static DBConnection db = new DBConnection();
	private DataSource dataFactory;
	
	public DBConnection() {
		try {
			Context ctx = new InitialContext();
			dataFactory = (DataSource) ctx.lookup("java:comp/env/jdbc/oracle11g");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static DBConnection getDb() {
		return db;
	}
	public DataSource getDataFactory() {
		return dataFactory;
	}
}