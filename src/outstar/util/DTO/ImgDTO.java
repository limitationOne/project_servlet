package outstar.util.DTO;

public class ImgDTO {
	private String name;
	private String path;
	private int pIdx;
	
	public ImgDTO() {}

	public ImgDTO(String name, String path, int pIdx) {
		super();
		this.name = name;
		this.path = path;
		this.pIdx = pIdx;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getpIdx() {
		return pIdx;
	}
	public void setpIdx(int pIdx) {
		this.pIdx = pIdx;
	}
}