//package outstar.util;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//
//import org.apache.tomcat.util.http.fileupload.FileItem;
//import org.apache.tomcat.util.http.fileupload.FileUploadException;
//import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
//import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
//
//
///**
// * 
// * Servlet implementation class Upload
// * 
// */
//@WebServlet("/Upload")
//public class Upload extends HttpServlet {
//
//	Logger logger = LogManager.getLogger(this.getClass());
//
//	/**
//	 * 
//	 * @see HttpServlet#HttpServlet()
//	 * 
//	 */
//	public Upload() {
//		super();
//	}
//
//	/**
//	 * 
//	 * @see HttpServlet#doGet(HttpServletRequest request,
//	 *      HttpServletResponse response)
//	 * 
//	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//	}
//
//	/**
//	 * 
//	 * @see HttpServlet#doPost(HttpServletRequest request,
//	 *      HttpServletResponse response)
//	 * 
//	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		// dopost로 넘어올 터니 여기에 코딩
//
//		DiskFileItemFactory factory = new DiskFileItemFactory();
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmssS"); // 중복파일명
//																		// 제외를
//																		// 위한
//																		// 셋팅
//		// 기본 경로 지정
//		String path = "c:\\upload";
//
//		// path 에 임시파일이 저장됨
//
//		// 기본으로는 File repository = (File)
//		// servletContext.getAttribute("javax.servlet.context.tempdir") 에서
//		// 구해온 곳에 저장됨
//
//		File repository = new File(path);
//
//		factory.setRepository(repository);
//
//		ServletFileUpload upload = new ServletFileUpload(factory);
//
//		try {
//			// parseRequest 하는 순간 upload 폴더에 임시파일들이 쌓임
//
//			List<FileItem> items = upload.parseRequest(request);
//
//			for (FileItem f : items) {
//
//				logger.debug("=====fileItem : \n" + f);
//
//				if (f.getName() == null) continue;
//
//				String tmpFileNm = f.getName().split("\\.")[0];
//				String tmpExtNm = "";
//
//				if (f.getName().split("\\.").length > 1) {
//					tmpExtNm = "." + f.getName().split("\\.")[1];
//				}
//
//				String uploadFileNm = tmpFileNm + "_" + sdf.format(new Date()) + tmpExtNm;
//
//				// 바로 파일로 저장
//				File uploadFile = new File(path + "\\" + uploadFileNm);
//
//				logger.debug("=====f.getName : " + f.getName());
//				logger.debug("=====uploadFile : " + (path + "\\" + f.getName()));
//
//				if (f.isFormField() == false) { // file 객체 필드
//
//					if (f.getName() != null && f.getName().equals("") == false) {
//
//						// 임시file 삭제는 FileCleaner 를 통해 삭제되는데
//						// 저장된 java.io.File 객체가 가비지컬렉터 되면 작동한다고 하는 것 같음
//
//						f.write(uploadFile);
//
//						// 스트림 처리
//
//						/*
//						 * 
//						 * InputStream uploadStream = f.getInputStream();
//						 * 
//						 * 
//						 * 
//						 * File uploadFile = new File(path + "\\" +
//						 * f.getName());
//						 * 
//						 * FileOutputStream outputStream = new
//						 * FileOutputStream(uploadFile);
//						 * 
//						 * 
//						 * 
//						 * byte[] buf = new byte[1024];
//						 * 
//						 * int len;
//						 * 
//						 * 
//						 * 
//						 * while( (len = uploadStream.read(buf)) > 0 ) {
//						 * 
//						 * outputStream.write(buf);
//						 * 
//						 * }
//						 * 
//						 * 
//						 * 
//						 * outputStream.close();
//						 * 
//						 * uploadStream.close();
//						 * 
//						 */
//					} else { // f.getName() != null &&
//								// f.getName().equals("") == false
//						logger.debug("name is null");
//					}
//				} else { // 일반 필드
//					logger.debug("파일필드 아님");
//				}
//			}
//			logger.info("=====items : \n" + items);
//
//			// 성공 메시지
//			request.setAttribute("logs", "성공하였습니다.");
//
//			// 처리 완료 후 forward처리
//			request.getRequestDispatcher("index.jsp").forward(request, response);
//		} catch (FileUploadException e) {
//			// logger.error(e, e);
//			e.printStackTrace();
//		} catch (Exception e) {
//				e.printStackTrace();
//		}
//	}
//}