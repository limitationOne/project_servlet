package outstar.util;

import java.io.File;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;

import outstar.db.DBConnection;
import outstar.util.DTO.ImgDTO;

public class UtilImg {
	private final static String PATH_RESOURCE = "../.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/OutStar/resource/userImg/";
	private final static String PATH_INSERT_DB = "../../resource/userImg/";
	
	public UtilImg() {}
	
	public boolean imgUpLoad(DiskFileItemFactory factory, ArrayList<FileItem> items, String pIdx) {
//	public boolean imgUpLoad(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("imgUpLoad()");
		
		boolean successCheck = false;
		String sql = "insert into img values (?,?)";
		String uploadFileNm = "";
		File uploadFile = null;
		
//		DiskFileItemFactory factory = null;
//		ServletFileUpload upLoad = null;
//		ArrayList<FileItem> items = null;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try{
//			factory = new DiskFileItemFactory();
//			upLoad = new ServletFileUpload(factory);
//			items = (ArrayList<FileItem>) upLoad.parseRequest(new ServletRequestContext(request));
//			
//			Iterator<FileItem> iter = items.iterator();
//			
//			// 占쏙옙占쏙옙 확占쏙옙
//			while (iter.hasNext()) {
//			    FileItem item = (FileItem) iter.next();
//			    
//			    if (item.isFormField()) {
//			      pIdx = item.getString().replaceAll(" ", "");
//			    }
//			}
			
			if(pIdx == null || "".equals(pIdx) ) return successCheck;
		
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmssS");
			File repository = new File(PATH_RESOURCE);
			
			if(!repository.exists()) repository.mkdirs();
			
			factory.setRepository(repository);
			
			conn = DBConnection.getDb().getDataFactory().getConnection();
			conn.setAutoCommit(false);
			
			for(FileItem f : items) {
				if(f.getName() == null ) continue;
				
//				String tmpFileNm = f.getName().split("\\.")[0];
				String tmpExtNm = "";
				
				if(f.getName().split("\\.").length > 1) {
					tmpExtNm = "." + f.getName().split("\\.")[1];
				}
				
				if("".equals(tmpExtNm.replaceAll(" ", ""))) continue;
				
				uploadFileNm = sdf.format(new Date()) + tmpExtNm;
				
				// 占쌕뤄옙 占쏙옙占싹뤄옙 占쏙옙占쏙옙
				uploadFile = new File(PATH_RESOURCE + uploadFileNm);

				// file 占쏙옙체 占십듸옙 확占쏙옙
				if(f.isFormField() == true) continue;
				
				if( !(f.getName() != null && f.getName().equals("") == false) ) {
					continue;
				}
				// 占쌈쏙옙file 占쏙옙占쏙옙占쏙옙 FileCleaner 占쏙옙 占쏙옙占쏙옙 占쏙옙占쏙옙占실는듸옙 
				// 占쏙옙占쏙옙占� java.io.File 占쏙옙체占쏙옙 占쏙옙占쏙옙占쏙옙占시뤄옙占쏙옙 占실몌옙 占쌜듸옙占싼다곤옙 占싹댐옙 占쏙옙 占쏙옙占쏙옙
				f.write(uploadFile);
				
				if(pstmt != null) pstmt.close();
				
				pstmt = conn.prepareStatement(sql);
				
				pstmt.setString(1, PATH_INSERT_DB + uploadFileNm);
				pstmt.setInt(2, Integer.parseInt(pIdx));
				
				pstmt.executeUpdate();
			}
			conn.commit();
			
			successCheck = true;
			
		} catch (Exception e) {
			if(uploadFile.exists()) uploadFile.delete();
			e.printStackTrace();
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			closeAll(rs, null, pstmt, conn);
		}
		return successCheck;
	}
	
	public ArrayList<ImgDTO> imgRead(String pIdx) {
		ArrayList<ImgDTO> list = new ArrayList<ImgDTO>();
		
		if(pIdx == null || "".equals(pIdx.replaceAll(" ", "")) ) return list;
		
		String sql = "select * from img where pIdx=?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try{
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, Integer.parseInt(pIdx));
			
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				String path = rs.getString("path");
				String[] pathSplit = path.split("/", path.lastIndexOf("/"));
				String name = pathSplit[pathSplit.length - 1];
				
				list.add(new ImgDTO(name, path, rs.getInt("pIdx")));
			}
		} catch (Exception e) {
			e.printStackTrace();
			imgDelete(pIdx);
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			closeAll(rs, null, pstmt, conn);
		}
		return list;
	}
	
	public boolean imgDelete(String pIdx){
		System.out.println("imgDelete()");
		
		boolean deleteCheck = false;
		
		if(pIdx == null || "".equals(pIdx.replaceAll(" ", "") )) return deleteCheck;
		
		ArrayList<ImgDTO> list = imgRead(pIdx);
		
		// img 占쏙옙占쏙옙 占쏙옙占쏙옙
		for(ImgDTO dto : list){
			String[] pathSplit = dto.getPath().split("/", dto.getPath().lastIndexOf("/"));
			String name = pathSplit[pathSplit.length - 1];
			
			System.out.println(PATH_RESOURCE + name);
			
			File file = new File(PATH_RESOURCE + name);
			
			// 占쏙옙占쏙옙 확占쏙옙
			if(!file.exists()) continue;
			
			// 占쏙옙占쏙옙 占쏙옙占쏙옙 확占쏙옙
			if(!file.delete()) continue;
		}
		
		String sql = "delete from img where pIdx=?";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DBConnection.getDb().getDataFactory().getConnection();
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, Integer.parseInt(pIdx));
			
			pstmt.executeUpdate();
			
			deleteCheck = true;
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeAll(rs, null, pstmt, conn);
		}
		return deleteCheck;
	}
	
	private void closeAll(ResultSet rs, CallableStatement cstmt, PreparedStatement pstmt, Connection conn){
		try {
			if(rs != null) rs.close();
			if(pstmt != null) pstmt.close();
			if(conn != null) conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}