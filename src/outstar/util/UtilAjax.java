package outstar.util;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

public class UtilAjax {
	public UtilAjax(Object data, HttpServletResponse response) {
		PrintWriter out = null;
		try {
//			response.setContentType("text/javascript");
			out = response.getWriter();
			out.print(data);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.flush();
			out.close();
		}
	}
}