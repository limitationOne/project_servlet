create table UserLogin(
	UserKey VARCHAR(150) not null primary key,
	UserID VARCHAR(15) not null unique,
	Password VARCHAR(20) not null
);

drop table UserLogin;



create table UserDetail(
	UserKey VARCHAR(40) not null primary key,
	UserName VARCHAR(15) not null unique,
	UserMail VARCHAR(40),
	UserPhone number(11),
	UserArea VARCHAR(4),
	UserGender number(1),
	UserType VARCHAR(150) not null
);

drop table UserDetail;

ALTER TABLE UserDetail
ADD CONSTRAINT fk_UserKey FOREIGN KEY(UserKey)
REFERENCEs UserDetail(UserKey);



